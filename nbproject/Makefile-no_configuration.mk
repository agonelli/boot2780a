#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-no_configuration.mk)" "nbproject/Makefile-local-no_configuration.mk"
include nbproject/Makefile-local-no_configuration.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=no_configuration
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/Boot2780A.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/Boot2780A.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=Source/HWTIMER.c Source/HWUART.c Source/HWI2C.c Source/HWGPIO.c Source/NVMem.c Source/USB/usb_msd_bootloader.c Source/lcd20x4.c Source/ee.c Source/protocolUtils.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/Source/HWTIMER.o ${OBJECTDIR}/Source/HWUART.o ${OBJECTDIR}/Source/HWI2C.o ${OBJECTDIR}/Source/HWGPIO.o ${OBJECTDIR}/Source/NVMem.o ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o ${OBJECTDIR}/Source/lcd20x4.o ${OBJECTDIR}/Source/ee.o ${OBJECTDIR}/Source/protocolUtils.o
POSSIBLE_DEPFILES=${OBJECTDIR}/Source/HWTIMER.o.d ${OBJECTDIR}/Source/HWUART.o.d ${OBJECTDIR}/Source/HWI2C.o.d ${OBJECTDIR}/Source/HWGPIO.o.d ${OBJECTDIR}/Source/NVMem.o.d ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o.d ${OBJECTDIR}/Source/lcd20x4.o.d ${OBJECTDIR}/Source/ee.o.d ${OBJECTDIR}/Source/protocolUtils.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/Source/HWTIMER.o ${OBJECTDIR}/Source/HWUART.o ${OBJECTDIR}/Source/HWI2C.o ${OBJECTDIR}/Source/HWGPIO.o ${OBJECTDIR}/Source/NVMem.o ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o ${OBJECTDIR}/Source/lcd20x4.o ${OBJECTDIR}/Source/ee.o ${OBJECTDIR}/Source/protocolUtils.o

# Source Files
SOURCEFILES=Source/HWTIMER.c Source/HWUART.c Source/HWI2C.c Source/HWGPIO.c Source/NVMem.c Source/USB/usb_msd_bootloader.c Source/lcd20x4.c Source/ee.c Source/protocolUtils.c



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-no_configuration.mk dist/${CND_CONF}/${IMAGE_TYPE}/Boot2780A.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MX250F256H
MP_LINKER_FILE_OPTION=,--script="btl_32MX250F256H_generic.ld"
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/Source/HWTIMER.o: Source/HWTIMER.c  .generated_files/53b6c329346ded53bbb8c3657f0da8765a88988.flag .generated_files/c67b10cc573849cce125cc5e50d2afd84043013c.flag
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/HWTIMER.o.d 
	@${RM} ${OBJECTDIR}/Source/HWTIMER.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/HWTIMER.o.d" -o ${OBJECTDIR}/Source/HWTIMER.o Source/HWTIMER.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWUART.o: Source/HWUART.c  .generated_files/89a42fdd020fcc932e0d54bf8f2afec7f3054de2.flag .generated_files/c67b10cc573849cce125cc5e50d2afd84043013c.flag
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/HWUART.o.d 
	@${RM} ${OBJECTDIR}/Source/HWUART.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/HWUART.o.d" -o ${OBJECTDIR}/Source/HWUART.o Source/HWUART.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWI2C.o: Source/HWI2C.c  .generated_files/3d9c22b1e91e782d66f188ce8850f9802970b518.flag .generated_files/c67b10cc573849cce125cc5e50d2afd84043013c.flag
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/HWI2C.o.d 
	@${RM} ${OBJECTDIR}/Source/HWI2C.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/HWI2C.o.d" -o ${OBJECTDIR}/Source/HWI2C.o Source/HWI2C.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWGPIO.o: Source/HWGPIO.c  .generated_files/dac8f2554ca8ba540b1b020aa0b7bfbdfdb815e.flag .generated_files/c67b10cc573849cce125cc5e50d2afd84043013c.flag
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/HWGPIO.o.d 
	@${RM} ${OBJECTDIR}/Source/HWGPIO.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/HWGPIO.o.d" -o ${OBJECTDIR}/Source/HWGPIO.o Source/HWGPIO.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/NVMem.o: Source/NVMem.c  .generated_files/8c497765d81e05fc345483966327bb8097753a12.flag .generated_files/c67b10cc573849cce125cc5e50d2afd84043013c.flag
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/NVMem.o.d 
	@${RM} ${OBJECTDIR}/Source/NVMem.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/NVMem.o.d" -o ${OBJECTDIR}/Source/NVMem.o Source/NVMem.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/USB/usb_msd_bootloader.o: Source/USB/usb_msd_bootloader.c  .generated_files/a264fb48e2f6f086dfa335bebefaff603da83b3e.flag .generated_files/c67b10cc573849cce125cc5e50d2afd84043013c.flag
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/USB/usb_msd_bootloader.o.d" -o ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o Source/USB/usb_msd_bootloader.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/lcd20x4.o: Source/lcd20x4.c  .generated_files/5ada138c858821fcc261a48ffaf2510f326ecd38.flag .generated_files/c67b10cc573849cce125cc5e50d2afd84043013c.flag
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/lcd20x4.o.d 
	@${RM} ${OBJECTDIR}/Source/lcd20x4.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/lcd20x4.o.d" -o ${OBJECTDIR}/Source/lcd20x4.o Source/lcd20x4.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/ee.o: Source/ee.c  .generated_files/5f71f955857455e2ee5a6852a10e8ce3c8b7c1fd.flag .generated_files/c67b10cc573849cce125cc5e50d2afd84043013c.flag
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/ee.o.d 
	@${RM} ${OBJECTDIR}/Source/ee.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/ee.o.d" -o ${OBJECTDIR}/Source/ee.o Source/ee.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/protocolUtils.o: Source/protocolUtils.c  .generated_files/fedb6c2c121c36c3b4ec84dbaf418481ed114be2.flag .generated_files/c67b10cc573849cce125cc5e50d2afd84043013c.flag
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/protocolUtils.o.d 
	@${RM} ${OBJECTDIR}/Source/protocolUtils.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/protocolUtils.o.d" -o ${OBJECTDIR}/Source/protocolUtils.o Source/protocolUtils.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
else
${OBJECTDIR}/Source/HWTIMER.o: Source/HWTIMER.c  .generated_files/30c1689ad7ddbbffa0e2d3d12d0535bb1f505cc4.flag .generated_files/c67b10cc573849cce125cc5e50d2afd84043013c.flag
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/HWTIMER.o.d 
	@${RM} ${OBJECTDIR}/Source/HWTIMER.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/HWTIMER.o.d" -o ${OBJECTDIR}/Source/HWTIMER.o Source/HWTIMER.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWUART.o: Source/HWUART.c  .generated_files/2be663e1bcec459ddb360db19c0d8411bd1fccef.flag .generated_files/c67b10cc573849cce125cc5e50d2afd84043013c.flag
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/HWUART.o.d 
	@${RM} ${OBJECTDIR}/Source/HWUART.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/HWUART.o.d" -o ${OBJECTDIR}/Source/HWUART.o Source/HWUART.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWI2C.o: Source/HWI2C.c  .generated_files/9f7511b60f71a6fe366f456f976da5878bc6cbae.flag .generated_files/c67b10cc573849cce125cc5e50d2afd84043013c.flag
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/HWI2C.o.d 
	@${RM} ${OBJECTDIR}/Source/HWI2C.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/HWI2C.o.d" -o ${OBJECTDIR}/Source/HWI2C.o Source/HWI2C.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWGPIO.o: Source/HWGPIO.c  .generated_files/9554cfb2ad91f2eaa2fb7d12a39199c658fc034b.flag .generated_files/c67b10cc573849cce125cc5e50d2afd84043013c.flag
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/HWGPIO.o.d 
	@${RM} ${OBJECTDIR}/Source/HWGPIO.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/HWGPIO.o.d" -o ${OBJECTDIR}/Source/HWGPIO.o Source/HWGPIO.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/NVMem.o: Source/NVMem.c  .generated_files/52f46ca3e6972e2439166d156aa0e7536f65bbda.flag .generated_files/c67b10cc573849cce125cc5e50d2afd84043013c.flag
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/NVMem.o.d 
	@${RM} ${OBJECTDIR}/Source/NVMem.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/NVMem.o.d" -o ${OBJECTDIR}/Source/NVMem.o Source/NVMem.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/USB/usb_msd_bootloader.o: Source/USB/usb_msd_bootloader.c  .generated_files/aba3671a7a8e009acd23a0910c3b71b46d093f75.flag .generated_files/c67b10cc573849cce125cc5e50d2afd84043013c.flag
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/USB/usb_msd_bootloader.o.d" -o ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o Source/USB/usb_msd_bootloader.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/lcd20x4.o: Source/lcd20x4.c  .generated_files/efddf1b4fe606a7c56b2ec98c53a23cc9e1763be.flag .generated_files/c67b10cc573849cce125cc5e50d2afd84043013c.flag
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/lcd20x4.o.d 
	@${RM} ${OBJECTDIR}/Source/lcd20x4.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/lcd20x4.o.d" -o ${OBJECTDIR}/Source/lcd20x4.o Source/lcd20x4.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/ee.o: Source/ee.c  .generated_files/674bb372f87d3e74f668af1d7823c04f55fcb49d.flag .generated_files/c67b10cc573849cce125cc5e50d2afd84043013c.flag
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/ee.o.d 
	@${RM} ${OBJECTDIR}/Source/ee.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/ee.o.d" -o ${OBJECTDIR}/Source/ee.o Source/ee.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/protocolUtils.o: Source/protocolUtils.c  .generated_files/910c3d245f23f8c2f6e356e08d11ec691276eec3.flag .generated_files/c67b10cc573849cce125cc5e50d2afd84043013c.flag
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/protocolUtils.o.d 
	@${RM} ${OBJECTDIR}/Source/protocolUtils.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -DOMIT_CONFIGURATIONS -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/protocolUtils.o.d" -o ${OBJECTDIR}/Source/protocolUtils.o Source/protocolUtils.c    -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)    
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/Boot2780A.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    btl_32MX250F256H_generic.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -g   -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/Boot2780A.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)      -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,-D=__DEBUG_D,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map" 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/Boot2780A.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   btl_32MX250F256H_generic.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/Boot2780A.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_no_configuration=$(CND_CONF)    $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map" 
	${MP_CC_DIR}\\xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/Boot2780A.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/no_configuration
	${RM} -r dist/no_configuration

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
