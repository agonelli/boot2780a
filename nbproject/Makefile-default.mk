#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=${DISTDIR}/boot2780a.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=${DISTDIR}/boot2780a.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=Source/HWTIMER.c Source/HWUART.c Source/HWI2C.c Source/HWGPIO.c Source/NVMem.c Source/USB/usb_msd_bootloader.c Source/lcd20x4.c Source/ee.c Source/protocolUtils.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/Source/HWTIMER.o ${OBJECTDIR}/Source/HWUART.o ${OBJECTDIR}/Source/HWI2C.o ${OBJECTDIR}/Source/HWGPIO.o ${OBJECTDIR}/Source/NVMem.o ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o ${OBJECTDIR}/Source/lcd20x4.o ${OBJECTDIR}/Source/ee.o ${OBJECTDIR}/Source/protocolUtils.o
POSSIBLE_DEPFILES=${OBJECTDIR}/Source/HWTIMER.o.d ${OBJECTDIR}/Source/HWUART.o.d ${OBJECTDIR}/Source/HWI2C.o.d ${OBJECTDIR}/Source/HWGPIO.o.d ${OBJECTDIR}/Source/NVMem.o.d ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o.d ${OBJECTDIR}/Source/lcd20x4.o.d ${OBJECTDIR}/Source/ee.o.d ${OBJECTDIR}/Source/protocolUtils.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/Source/HWTIMER.o ${OBJECTDIR}/Source/HWUART.o ${OBJECTDIR}/Source/HWI2C.o ${OBJECTDIR}/Source/HWGPIO.o ${OBJECTDIR}/Source/NVMem.o ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o ${OBJECTDIR}/Source/lcd20x4.o ${OBJECTDIR}/Source/ee.o ${OBJECTDIR}/Source/protocolUtils.o

# Source Files
SOURCEFILES=Source/HWTIMER.c Source/HWUART.c Source/HWI2C.c Source/HWGPIO.c Source/NVMem.c Source/USB/usb_msd_bootloader.c Source/lcd20x4.c Source/ee.c Source/protocolUtils.c



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk ${DISTDIR}/boot2780a.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MX250F256H
MP_LINKER_FILE_OPTION=,--script="btl_32MX250F256H_generic.ld"
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/Source/HWTIMER.o: Source/HWTIMER.c  .generated_files/flags/default/c9497fbc4ac6fc27600e05721138ae884fb10b43 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/HWTIMER.o.d 
	@${RM} ${OBJECTDIR}/Source/HWTIMER.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/HWTIMER.o.d" -o ${OBJECTDIR}/Source/HWTIMER.o Source/HWTIMER.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWUART.o: Source/HWUART.c  .generated_files/flags/default/2d8428062cd9817f09493fdcc23aa89fa95bfe06 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/HWUART.o.d 
	@${RM} ${OBJECTDIR}/Source/HWUART.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/HWUART.o.d" -o ${OBJECTDIR}/Source/HWUART.o Source/HWUART.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWI2C.o: Source/HWI2C.c  .generated_files/flags/default/e2cf92eb6a5d4f1ad5de0e13bd21f79a558656b7 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/HWI2C.o.d 
	@${RM} ${OBJECTDIR}/Source/HWI2C.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/HWI2C.o.d" -o ${OBJECTDIR}/Source/HWI2C.o Source/HWI2C.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWGPIO.o: Source/HWGPIO.c  .generated_files/flags/default/b533fbb53505fe25c3dabcf47c46e3cf9601b1d0 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/HWGPIO.o.d 
	@${RM} ${OBJECTDIR}/Source/HWGPIO.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/HWGPIO.o.d" -o ${OBJECTDIR}/Source/HWGPIO.o Source/HWGPIO.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/NVMem.o: Source/NVMem.c  .generated_files/flags/default/fc564d1be007375b867250319781fca50f5279 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/NVMem.o.d 
	@${RM} ${OBJECTDIR}/Source/NVMem.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/NVMem.o.d" -o ${OBJECTDIR}/Source/NVMem.o Source/NVMem.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/USB/usb_msd_bootloader.o: Source/USB/usb_msd_bootloader.c  .generated_files/flags/default/2da4540e13d723afae1e5d7ab23068430f06defe .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/USB/usb_msd_bootloader.o.d" -o ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o Source/USB/usb_msd_bootloader.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/lcd20x4.o: Source/lcd20x4.c  .generated_files/flags/default/1eb5201b223ec096ddaf7396a00f688b95beaa05 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/lcd20x4.o.d 
	@${RM} ${OBJECTDIR}/Source/lcd20x4.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/lcd20x4.o.d" -o ${OBJECTDIR}/Source/lcd20x4.o Source/lcd20x4.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/ee.o: Source/ee.c  .generated_files/flags/default/a2a94f2fd09c68a04be5ab1f51d6a7cb8bf02952 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/ee.o.d 
	@${RM} ${OBJECTDIR}/Source/ee.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/ee.o.d" -o ${OBJECTDIR}/Source/ee.o Source/ee.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/protocolUtils.o: Source/protocolUtils.c  .generated_files/flags/default/76c72c9379ce95042562432cb5055f8a47aa7f02 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/protocolUtils.o.d 
	@${RM} ${OBJECTDIR}/Source/protocolUtils.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/protocolUtils.o.d" -o ${OBJECTDIR}/Source/protocolUtils.o Source/protocolUtils.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
else
${OBJECTDIR}/Source/HWTIMER.o: Source/HWTIMER.c  .generated_files/flags/default/3cf31dd5c10997e3fd69d127de9008d83f5ef3ab .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/HWTIMER.o.d 
	@${RM} ${OBJECTDIR}/Source/HWTIMER.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/HWTIMER.o.d" -o ${OBJECTDIR}/Source/HWTIMER.o Source/HWTIMER.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWUART.o: Source/HWUART.c  .generated_files/flags/default/2556ed6247128159851438faa841079644d7e69b .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/HWUART.o.d 
	@${RM} ${OBJECTDIR}/Source/HWUART.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/HWUART.o.d" -o ${OBJECTDIR}/Source/HWUART.o Source/HWUART.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWI2C.o: Source/HWI2C.c  .generated_files/flags/default/20c0974f23c078aed7214783d1c500a38653039b .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/HWI2C.o.d 
	@${RM} ${OBJECTDIR}/Source/HWI2C.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/HWI2C.o.d" -o ${OBJECTDIR}/Source/HWI2C.o Source/HWI2C.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/HWGPIO.o: Source/HWGPIO.c  .generated_files/flags/default/4466b70df3e71a62011c310b9969a747c1a6644d .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/HWGPIO.o.d 
	@${RM} ${OBJECTDIR}/Source/HWGPIO.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/HWGPIO.o.d" -o ${OBJECTDIR}/Source/HWGPIO.o Source/HWGPIO.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/NVMem.o: Source/NVMem.c  .generated_files/flags/default/e9bc60239c5fd96598c76258520e5017da92f500 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/NVMem.o.d 
	@${RM} ${OBJECTDIR}/Source/NVMem.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/NVMem.o.d" -o ${OBJECTDIR}/Source/NVMem.o Source/NVMem.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/USB/usb_msd_bootloader.o: Source/USB/usb_msd_bootloader.c  .generated_files/flags/default/9a4c8d18b142babb66c86f89e929016425d79087 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source/USB" 
	@${RM} ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o.d 
	@${RM} ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/USB/usb_msd_bootloader.o.d" -o ${OBJECTDIR}/Source/USB/usb_msd_bootloader.o Source/USB/usb_msd_bootloader.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/lcd20x4.o: Source/lcd20x4.c  .generated_files/flags/default/c7dd3739f4ee74e0dc467a34bcc6dbdaef3d4098 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/lcd20x4.o.d 
	@${RM} ${OBJECTDIR}/Source/lcd20x4.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/lcd20x4.o.d" -o ${OBJECTDIR}/Source/lcd20x4.o Source/lcd20x4.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/ee.o: Source/ee.c  .generated_files/flags/default/a42691f1f0cc40d7b7cb00c9ead533d8e4054475 .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/ee.o.d 
	@${RM} ${OBJECTDIR}/Source/ee.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/ee.o.d" -o ${OBJECTDIR}/Source/ee.o Source/ee.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
${OBJECTDIR}/Source/protocolUtils.o: Source/protocolUtils.c  .generated_files/flags/default/844b69f6e96b2b114df6a2070816265cfcf0f04e .generated_files/flags/default/52c640c780c555f4d7569469c02cd936b1f38f4a
	@${MKDIR} "${OBJECTDIR}/Source" 
	@${RM} ${OBJECTDIR}/Source/protocolUtils.o.d 
	@${RM} ${OBJECTDIR}/Source/protocolUtils.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DTRANSPORT_LAYER_USB_HOST -DDEMO_BOARD_USB_STARTER_KIT -DONLY_BASIC_FUNCTIONS -D_DEGBUG -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -I"Include/USB" -I"Include/MDD File Systems" -I"Include" -I"Include/HardwareProfile" -I"Include/Framework" -I"z:/msp/swlib/clib" -I"C:/Program Files (x86)/Microchip/xc32/v1.34" -I"source" -MP -MMD -MF "${OBJECTDIR}/Source/protocolUtils.o.d" -o ${OBJECTDIR}/Source/protocolUtils.o Source/protocolUtils.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${DISTDIR}/boot2780a.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    btl_32MX250F256H_generic.ld
	@${MKDIR} ${DISTDIR} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -g -mdebugger -D__MPLAB_DEBUGGER_ICD3=1 -mprocessor=$(MP_PROCESSOR_OPTION)  -o ${DISTDIR}/boot2780a.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)   -mreserve=data@0x0:0x1FC -mreserve=boot@0x1FC00490:0x1FC00BEF  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,-D=__DEBUG_D,--defsym=__MPLAB_DEBUGGER_ICD3=1,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map" 
	
else
${DISTDIR}/boot2780a.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   btl_32MX250F256H_generic.ld
	@${MKDIR} ${DISTDIR} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o ${DISTDIR}/boot2780a.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map" 
	${MP_CC_DIR}\\xc32-bin2hex ${DISTDIR}/boot2780a.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${OBJECTDIR}
	${RM} -r ${DISTDIR}

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
