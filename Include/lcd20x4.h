#ifndef _DECL_LCD20X4
#define _DECL_LCD20X4 extern


#endif


_DECL_LCD20X4 void printClcd(byte add,const  char * p);
_DECL_LCD20X4 void printSlcd(byte add,char * p);
_DECL_LCD20X4 void waitbflcd(void);
_DECL_LCD20X4 void writelcd(byte flcd,byte d);
_DECL_LCD20X4 void printwlcd(byte add,word w,byte flags);
_DECL_LCD20X4 void printdwlcd(byte add,dword dw,byte flags);
_DECL_LCD20X4 void writelcd(byte flcd,byte d);
_DECL_LCD20X4 void clrlcd(void);
_DECL_LCD20X4 void printFlashLCD(word add,byte lcdadd,byte nchar);
//_DECL_LCD20X4 void setSymbolLCD(void);
_DECL_LCD20X4 void setCursor(byte add,byte flags);
_DECL_LCD20X4 void bar(byte min,byte max,byte val);
_DECL_LCD20X4 void setCursor(byte add,byte flag);
_DECL_LCD20X4 byte tempbuf[7];
_DECL_LCD20X4 void printEElcd(word add,byte lcdadd,byte nchar);
_DECL_LCD20X4 void printdataora(byte add);



