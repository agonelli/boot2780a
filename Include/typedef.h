/* 
 * File:   typedef.h
 * Author: agonelli
 *
 * Created on 22 maggio 2015, 9.53
 */

#ifndef TYPEDEF_H
#define	TYPEDEF_H

#ifdef	__cplusplus
extern "C" {
#endif

//*** Tipi di Dato
typedef unsigned char byte;
typedef unsigned short word;
typedef unsigned long dword;

// righe display
typedef enum {
LINGUA,         //0	//
ACCENSIONE1,    //1	//RVC
ACCENSIONE2,    //2	//RVC
GASNAME,        //questo valore � speciale e non deve essere toccato
STANDBY1,       //4	//rvc
STANDBY2,       //5	//rvc
OLIO4,          //6     //rvc olio si
RECUPERO1,      //7	//rvc
AL_P1_1,	//8	//rvc
AL_P1_1_1,	//9	//rvc
RECUPERO3,  	//10	//rvc
AL_PM,		//11	//rvc
AL_PM_1	,	//12	//rvc
TIMEOUT ,	//13	//rvc
TIMEOUT_1 ,     //14	//rvc
AL_MAXREPREC,   //15	//rvc
AL_MAXREPREC_1, // 16	//rvc
ALPMOLIO,	//17	//rvc
ALPMOLIO_1,	//18	//rvc
ENTERCONTINUA,	//19	//rvc
MENUESCI,	//20
ALPROTHP,	//21	//rvc
ALPROTHP_1,	//22	//rvc
VUOTO1,		//23		//rvc
VUOTO2,		//24		//rvc
ALSTARTVAC,	//25		//rvc
ALSTARTVAC_1,	//26		//rvc
VUOTO3,		//27		//rvc
TESTVUOTO,	//28		//rvc
ALTESTVUOTO,	//29		//rvc
ALTESTVUOTO_1,  //30	//rvc
ENDVUOTO1,	//31		//rvc
ENDVUOTO2,	//32		//rvc
ALTMAXVAC,	//33	//rvc
ALTMAXVAC_1	,//34	//rvc
CARICA0,	//35		//rvc
OLIO0,          //36		//rvc
OLIO1,          //37		//rvc
OLIO2,          //38		//rvc
OLIO3,          //39		//rvc
CARICA3,	//40		//rvc
CARICA1_	,//41		//rvc
CARICA2,	//42		//rvc
MAINMENU,	//43		//rvc
MAINMENU_1,     //45	//rvc
MAINMENU_2,	//46		//rvc
DBPERS	,	//47		//rvc
DBPERS1,	//48
DBPERS2	,	//49		//rvc//rvc
DBPERS3	,	//50
DBPERS4	,	//50
TARGA0	,	//51		//rvc
MENUUDM1,	//52		//rvc
MENUUDM2,	//53		//rvc
DATAORA0,	//54		//rvc
DATAORA1,	//55		//rvc
CALIB1,		//56		//rvc
CALIB1_1,	//57		//rvc
CALIB2	,	//58		//RVC
CALIB2_1,	//59		//rvc
TARATURA0,	//60		//rvc
TARATURA1,	//61		//rvc
TARATURA2,	//62		//RVC
TARATURA3,	//63		//rvc
TARATURA4,	//64		//RVC
TERMINATO,	//65		//rvc
LAVAGGIO1,	//66		//rvc
EDIT1	,	//67		//rvc
MENUDATABASE,   //68		//rvc
FLOW_GAS,       //69		//rvc
UV2,         //	70
UV3,         //	71
PRECHARGE,	//72		//rvc
PRINTERCUPERO0, //73	//rvc
PRINTVUOTO0 ,   //74		//rvc
PRINTOLIO ,	//75		//rvc
PRINTCARICA,	//76		//rvc
OLIO5,          //77 olio no    //rvc
STRKG	,	//78		//rvc
STRKG_1	,	//79//rvc
STRGR	,	//80		//rvc
STRGR_1	,	//81		//rvc
STRMB	,	//82		//rvc
OLIO6	,	//83		//rvc
AZZCONT	,	//84		//rvc
ACCENSIONE,	//85		//rvc
RECUPERO,	//86		//rvc
VUOTO,		//87		//rvc
SEISICURO,	//88		//rvc
ALPOMP,		//91		//rvc
ALPOMP_1,	//92	//rvc
ALFILT,		//89		//rvc
ALFILT_1,	//90	//rvc
CICLOAUTO,	//93		//rvc
AUTO_1	,	//94		//rvc
TUTTO	,	//95		//rvc
STESSO	,	//96	//rvc
TARATURA5	,//97	//rvc
TESTRECUPERO,	//98	//rvc
SCARICOOLIO	,//99	//rvc
RECUPERO_TUTTO, //100 //rvc
RECUPEROESCLUSO, //101 //rvc
CICLILAVAGGIO , //102 //rvc
DBSTD  , 	//	103 //rvc
ALAUTO0 ,  	//	104 //rvc
ALAUTO0_1,      //	105 //rvc
ALOLIO0  , 	//	106 //rvc
ALOLIO0_1,	//	107 //rvc
DUE_TUBI,	// 08 //rvc
UN_TUBO	,	//	109 //rvc
START	,       //	110
CONT_OLIO,	//	111
CARICA,         //	112
MANUALE,	//	113
AZZERA,         //	114
E_ESP,          //	115
E_TOTCAR,       //	116
E_TOTREC,       //	117
E_TOTREDFILT,   //	118
E_TOTACC,       //	119
E_TOTVUOTO,     //	120
SERIALE_FILTRI, //	121
ERR_COD_FASE,   //	122
REC_PERSI,      //	123
CAMBIO_OLIO,    //	124
CAMBIO_OLIO2,   //      125
WARMUP,         //	126
UV1,         //	127
OLIOUV1,
OLIOUV2,
CODATT,
CLI1,        
CLI2,        
CLI3,        
CLI4,        
CLI5,        
CLI6,        
CLI7,        
CLI8,        
CLI9,        
CLI10,        
NofLine
} LineeLingue;


typedef enum {
	PiccolaEvo,
	VBus,
	NofVer21B0
} Ver21B0;

typedef enum {
	ec_OK,
	ec_Skip,
	ec_Alarm
} ExitCodes;

typedef struct{
	unsigned short tVuoto;
	unsigned char tTest;
	unsigned short tVuotoEffettivo;
	ExitCodes exitcode;
} t_Vuoto;


typedef struct{
	unsigned short	pRec;
	unsigned short pRecEffettivo;
	int pOilScar;
	ExitCodes exitcode;
} t_Recupero;


typedef struct{
	unsigned short pCar;
	unsigned short pCarEffettivo;
	unsigned short pOlioCar;
        unsigned short pUvCar;
	ExitCodes exitcode;
} t_Carica;


typedef struct{
	unsigned char nLavaggi;
	unsigned short tVuoto;
	unsigned char nlavaggieffettivi;
	unsigned char pOilScar;
	ExitCodes exitcode;
} t_Lavaggio;

typedef enum {
	FaseRecupero = 10,
	FaseVuoto,
	FaseCarica,
	FaseLavaggio
} Fasi;
typedef enum {
	REC_AUTO,
	REC_MAN,
    REC_LAV
} RecordType;

typedef enum {
        NormalBoot,
        UpdateFromFlash,
        CopyNewVersionToFlash,
        UpdateFromSecureVersion,
        PC_Link,
        Collaudo,
        LoadLanguage,
        LoadDefaultParam,
        UpdateFW_LoadLanguage,
        LoadGasParameter,
        CollaudoF2,
        SaveSecureVersionToFlash = 0xFF
} e21B0SD_BootSwitch;

typedef enum {
	HPLP,
	HP,
} Tubi;


typedef enum {
	pos_STX,
	pos_ETX,
	pos_Separatore,
} StartEndPos;

typedef enum {
    Grammi,
    Minuti,
    Ore
} t_um;


#ifdef	__cplusplus
}
#endif

#endif	/* TYPEDEF_H */

