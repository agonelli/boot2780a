/* 
 * File:   I2C.h
 * Author: andrea
 *
 * Created on 8 maggio 2013, 15.10
 */

#ifndef HWI2C_H
#define	HWI2C_H

#ifdef	__cplusplus
extern "C" {
#endif

    // EEPROM Constants
//#define EEPROM_I2C_BUS              I2C1
//#define EEPROM0_ADDRESS              0x50        // 0b1010000 Serial EEPROM address
//#define EEPROM1_ADDRESS              0x57        // 0b1010111 Serial EEPROM address
BOOL StartTransfer( BOOL restart ,unsigned char device );
BOOL TransmitOneByte( UINT8 data ,unsigned char device);
void StopTransfer( unsigned char device );

extern I2C_7_BIT_ADDRESS   SlaveAddress;

#ifdef	__cplusplus
}
#endif

#endif	/* I2C_H */

