/* 
 * File:   HWUART.h
 * Author: andrea
 *
 * Created on 7 giugno 2013, 15.41
 */

#ifndef HWUART_H
#define	HWUART_H

#ifdef	__cplusplus
extern "C" {
#endif

void initUart1(void);
void txCharUart1(unsigned char b);
void txUintUart1(unsigned int i);
unsigned char rxCharUart1(unsigned char * b);
unsigned char dataReadyUart1(void);
unsigned char readCharUart1(void);
byte rxb(unsigned short timeout);


//
//void initUart2(void);
//void txCharUart2(unsigned char b);
//void txUintUart2(unsigned int i);
//unsigned char rxCharUart2(unsigned char * b);
//void UART_CAP_Send(unsigned char * Buffer, unsigned short BufferLen);
//void initUart4(void);
//void txCharUart4(unsigned char b);

void set485tx(void);
void set485rx(void);


#ifdef	__cplusplus
}
#endif

#endif	/* HWUART_H */

