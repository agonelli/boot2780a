#ifndef _DECL_EE
#define _DECL_EE extern
#endif



_DECL_EE void WritePageEE(word add,byte * pbuf,byte pagelength);
_DECL_EE byte readByteEE(word add);
_DECL_EE void eetxb(byte eeb);
_DECL_EE byte erack(void);
_DECL_EE void ewack(void);
_DECL_EE byte eerxb(void);
//_DECL_EE void readPar(void);
_DECL_EE word readWordEE(word add);
_DECL_EE dword readDWordEE(word add);
_DECL_EE void refreshEE(void);
_DECL_EE void WriteWordEE(word add,word val);
_DECL_EE void WriteDWordEE(word add,dword val);
_DECL_EE void WriteByteEE(word add,byte val);


#define EEPROM_I2C_BUS              I2C2
#define EEPROM_ADDRESS              0x50        // 0b1010000 Serial EEPROM address
#define I2C_CLOCK_FREQ             5000
#define EEPROM0_ADDRESS              0x50        // 0b1010000 Serial EEPROM address
#define EEPROM1_ADDRESS              0x57        // 0b1010111 Serial EEPROM address

