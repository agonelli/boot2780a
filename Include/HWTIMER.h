/* 
 * File:   HWTIMER.h
 * Author: andrea
 *
 * Created on 12 marzo 2013, 8.03
 */

#ifndef HWTIMER_H
#define	HWTIMER_H

#ifdef	__cplusplus
extern "C" {
#endif

void SysDelayUs(unsigned short us );
//void Timer1Init(void);
void SysDelay05Us(void);
//void DelayMs(WORD ms);

//#define APS_IsStepTimerRun() T3CONbits.T3_ON
//#define APS_IsHeadOffTimerRun() T1CONbits.TMR1ON

#ifdef	__cplusplus
}
#endif

#endif	/* HWTIMER_H */

