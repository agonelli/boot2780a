#include"../Include/HWGPIO.h"
#include"hwtimer.h"
#include"global.h"

//extern volatile unsigned short usBuzzerTimer;

void initGPIO(void){
    CNPUCbits.CNPUC14=1;
    CNPUDbits.CNPUD8=1;
    CNPUDbits.CNPUD2=1;
    CNPUDbits.CNPUD9=1;
    CNPUGbits.CNPUG9=1;
    CNPUDbits.CNPUD7=1;
    CNPUDbits.CNPUD13=1;
    CNPUDbits.CNPUD15=1;
    CNPUBbits.CNPUB0=1;
    CNPUBbits.CNPUB1=1;
    
    PORTSetPinsDigitalOut(OUTUV_PORTID,OUTUV_PIN);
    PORTClearBits(OUTUV_PORTID, OUTUV_PIN);
    PORTSetPinsDigitalOut(OUTRF_PORTID,OUTRF_PIN);
    PORTClearBits(OUTRF_PORTID, OUTRF_PIN);
    PORTSetPinsDigitalOut(OUTLP_PORTID,OUTLP_PIN);
    PORTClearBits(OUTLP_PORTID, OUTLP_PIN);
    PORTSetPinsDigitalOut(FCS_PORTID,FCS_PIN);
    PORTSetBits(FCS_PORTID,FCS_PIN);
    PORTSetPinsDigitalOut(FMOSI_PORTID,FMOSI_PIN);
    PORTSetPinsDigitalOut(FCLOCK_PORTID,FCLOCK_PIN);
    PORTSetPinsDigitalIn(FMISO_PORTID,FMISO_PIN);
    PORTSetPinsDigitalIn(PT_PORTID,PT_PIN);
    PORTSetPinsDigitalOut(USBPWR_PORTID,USBPWR_PIN);
    PORTClearBits(USBPWR_PORTID, USBPWR_PIN);
    PORTSetPinsDigitalIn(U3RX_PORTID,U3RX_PIN);
    PORTSetPinsDigitalOut(OUTA3_PORTID,OUTA3_PIN);
    PORTClearBits(OUTA3_PORTID,OUTA3_PIN);
    PORTSetPinsDigitalIn(RB0_PORTID,RB0_PIN);
    PORTSetPinsDigitalIn(RB1_PORTID,RB1_PIN);
    PORTSetPinsAnalogIn(USBOC_PORTID, USBOC_PIN);
    PORTSetPinsDigitalOut(DIR485_PORTID,DIR485_PIN);
    PORTClearBits(DIR485_PORTID, DIR485_PIN);
    PORTSetPinsDigitalOut(U3TX_PORTID,U3TX_PIN);
    PORTSetPinsDigitalOut(LED2_PORTID,LED2_PIN);
    PORTClearBits(LED2_PORTID, LED2_PIN);
    PORTSetPinsAnalogIn(TEMPINT_PORTID, TEMPINT_PIN);
    PORTSetPinsAnalogIn(TEMPEXT_PORTID, TEMPEXT_PIN);
    PORTSetPinsDigitalOut(SCLK1_PORTID,SCLK1_PIN);
    
    PORTSetPinsAnalogIn(INTADC1_PORTID, INTADC1_PIN);
    PORTSetPinsAnalogIn(INTADC2_PORTID, INTADC2_PIN);
    PORTSetPinsAnalogIn(INTADC3_PORTID, INTADC3_PIN);
    
    PORTSetPinsDigitalOut(I2CSDA_PORTID,I2CSDA_PIN);
   // PORTSetBits(I2CSDA_PORTID,I2CSDA_PIN);
    PORTSetPinsDigitalOut(I2CSCL_PORTID,I2CSCL_PIN);
   // PORTSetBits(I2CSCL_PORTID,I2CSCL_PIN);
    
    PORTSetPinsDigitalIn(HWID_PORTID,HWID_PIN);
    PORTSetPinsDigitalOut(CSADC1_PORTID,CSADC1_PIN);
    PORTSetBits(CSADC1_PORTID,CSADC1_PIN);
    PORTSetPinsDigitalOut(CSADC2_PORTID,CSADC2_PIN);
    PORTSetBits(CSADC2_PORTID,CSADC2_PIN);
    PORTSetPinsDigitalOut(MOSI1_PORTID,MOSI1_PIN);
    PORTSetPinsDigitalIn(MISO1_PORTID,MISO1_PIN);
    PORTSetPinsDigitalIn(ADCRDY_PORTID,ADCRDY_PIN);
    PORTSetPinsDigitalOut(OUTHO_PORTID,OUTHO_PIN);
    PORTClearBits(OUTHO_PORTID,OUTHO_PIN);
    PORTSetPinsDigitalOut(CSADCPR_PORTID,CSADCPR_PIN);
    PORTSetBits(CSADCPR_PORTID,CSADCPR_PIN);
    PORTSetPinsDigitalIn(FANSIGN_PORTID,FANSIGN_PIN);
    PORTSetPinsDigitalIn(U1RX_PORTID,U1RX_PIN);
    PORTSetPinsDigitalOut(U1TX_PORTID,U1TX_PIN);
    PORTSetPinsDigitalOut(OUTA4_PORTID,OUTA4_PIN);
    PORTClearBits(OUTA4_PORTID, OUTA4_PIN);
    PORTSetPinsDigitalOut(OUTA5_PORTID,OUTA5_PIN);
    PORTClearBits(OUTA5_PORTID, OUTA5_PIN);
    PORTSetPinsDigitalOut(LED1_PORTID,LED1_PIN);
    PORTClearBits(LED1_PORTID, LED1_PIN);
    PORTSetPinsDigitalOut(I2CWC_PORTID,I2CWC_PIN);
    PORTSetBits(I2CWC_PORTID,I2CWC_PIN);
    PORTSetPinsDigitalOut(OUTVP_PORTID,OUTVP_PIN);
    PORTClearBits(OUTVP_PORTID, OUTVP_PIN);
    PORTSetPinsDigitalOut(OUTCP_PORTID,OUTCP_PIN);
    PORTClearBits(OUTCP_PORTID, OUTCP_PIN);
    PORTSetPinsDigitalOut(OUTA1_PORTID,OUTA1_PIN);
    PORTClearBits(OUTA1_PORTID, OUTA1_PIN);
    PORTSetPinsDigitalOut(OUTXP_PORTID,OUTXP_PIN);
    PORTClearBits(OUTXP_PORTID, OUTXP_PIN);
    PORTSetPinsDigitalOut(OUTA2_PORTID,OUTA2_PIN);
    PORTClearBits(OUTA2_PORTID, OUTA2_PIN);
    PORTSetPinsDigitalOut(OUTEO_PORTID,OUTEO_PIN);
    PORTClearBits(OUTEO_PORTID, OUTEO_PIN);
    PORTSetPinsDigitalOut(OUTNO_PORTID,OUTNO_PIN);
    PORTClearBits(OUTNO_PORTID, OUTNO_PIN);
    
//    PORTSetPinsDigitalOut(LED6_PORTID,LED6_PIN);
//    PORTSetPinsDigitalOut(BUZZER_PORTID,BUZZER_PIN);
 //   PORTSetPinsDigitalOut(PRNRESET_PORTID,PRNRESET_PIN);
    PORTSetBits(LED1_PORTID,LED1_PIN);
//    PORTSetBits(LED2_PORTID,LED2_PIN);
//    PORTSetBits(LED3_PORTID,LED3_PIN);
//    PORTSetBits(LED4_PORTID,LED4_PIN);
//    PORTSetBits(LED5_PORTID,LED5_PIN);
//    PORTSetBits(LED6_PORTID,LED6_PIN);
    EE_WC_High();
//    BUZZER_OFF();
    USBPWR_OFF();

}


