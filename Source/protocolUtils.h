
#ifndef _PROTOCOLUTILS_H   
#define _PROTOCOLUTILS_H


#ifdef __cplusplus
extern "C" {
#endif

byte protSendCommand(byte * cmdbuf, byte nchartx,word timeout, byte* rplbuf, word ncharrx,byte forceExit);
void testUSB(void);
void cmdUsbOn(void);
void cmdUsbOff(void);
void cmdUsbSt(byte * st);
byte cmdUsbOpenFile(char * filename);
byte cmdUsbAppendFile(byte * buf, byte nchar);
void cmdUsbCloseFile(void);
void cmdReset(void);
void cmdCheckAppValid(byte * res);
byte cmdUsbReadFile(char * filename);
void cmdUsbSearchWow(char * filename);
byte cmdUsbCopyFile(char * filename);
byte cmdUsbWaitCopyFile(void);
void cmdUsbJumpAppB(void);
void cmdCheckBootLoaderMode(byte * res);
void setLedsReadKey(byte bz,byte leds, byte * pkey, char * pencoder);
void readClock(byte *g,byte*m,byte*a,byte*h,byte*mi,byte*s);
void writeClock(byte *g,byte*m,byte*a,byte*h,byte*mi,byte*s);
void readFlash(byte * bufadd,dword flashadd,word nchar);
byte cmdReadBVers(byte * hw,byte * sw);

//Richieste da parte del MASTER
#define CHECKAPPBVALID  5
#define CHECKUSBPRESENT 10
#define SEARCHFILE      20
#define COPYFILE        30
#define WAITCOPYFILE    35
#define PROGRAM2770B    40
#define PROGRAM2770A    50
#define JUMPTOAPPA      200
#define JUMPTOAPPB      201


//STATI MACCHINA A STATI GestUpdate
#define ST_UPDATE_IDLE          0xee
#define ST_USBDETECT            1
#define ST_USB_NO_PRESENT       0
#define ST_USB_PRESENT          10
#define ST_USB_FS_INIT          20
#define ST_USB_FS_OK            20
#define ST_FINDFILE             25
#define ST_FILE_FOUND           21
#define ST_FILE_NOT_FOUND       22
#define ST_FILE_RECENT          23
#define ST_UPDATE_RUN           30
#define ST_PROGRAMB_END          201
#define ST_PROGRAMB_FAIL         202
#define ST_UPDATE_JUMP          210
#define ST_PROGRAM2770B         220
#define JUMPTOAPPA      200
#define JUMPTOAPPB      201

#define ADD_FILE_LANG   0
#define ADD_FILE_FWA        0x100000
#define ADD_FILE_FWA_16M    0x800000
#define ADD_FILE_FWB        0x200000
#define ADD_FILE_DB         0x300000
#define ADDFLASHEND         0x100000

#define WFLASMEM16M 0x01
#define HWGRDISPLAY 0x02

    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif
#endif 

/* *****************************************************************************
 End of File
 */
