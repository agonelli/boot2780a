



#define _DECL_EE
#include "global.h" 
#include "ee.h"  
//#include "varie.h" 
//#include "var.h" 
#include "define.h" 
#include "HWGPIO.h"
#include "pic32mx\include\peripheral\i2c.h"



//**************************************************************
//*** ***********************************************
//**************************************************************
void WriteDWordEE(word add,dword val){
    WriteByteEE(add,(val>>24)&0xff);
    WriteByteEE(add+1,(val>>16)&0xff);
    WriteByteEE(add+2,(val>>8)&0xff);
    WriteByteEE(add+3,val&0xff);
}

//**************************************************************
//***  scrittura byte in eeprom             **********************
//**************************************************************
void WriteByteEE(word add,byte val){
    unsigned char EepromAddress;
    I2C_7_BIT_ADDRESS   SlaveAddress;

    if(add&0x8000){
        EepromAddress=EEPROM1_ADDRESS;
        add&=(~0x8000);
    }else{
        EepromAddress=EEPROM0_ADDRESS;
    }

    EE_WC_Low();
    for(;;){
        I2C_FORMAT_7_BIT_ADDRESS(SlaveAddress, EepromAddress, I2C_WRITE);
        if( StartTransfer(FALSE,EEPROM_I2C_BUS) ){
            if (TransmitOneByte(SlaveAddress.byte,EEPROM_I2C_BUS)){
                if(I2CByteWasAcknowledged(EEPROM_I2C_BUS)){
                    if (TransmitOneByte((unsigned char)(add>>8),EEPROM_I2C_BUS)){
                        if(I2CByteWasAcknowledged(EEPROM_I2C_BUS)){
                            if (TransmitOneByte((unsigned char)(add&0xff),EEPROM_I2C_BUS)){
                                if(I2CByteWasAcknowledged(EEPROM_I2C_BUS)){
                                    if (TransmitOneByte(val,EEPROM_I2C_BUS)){
                                        if(I2CByteWasAcknowledged(EEPROM_I2C_BUS)){
                                            StopTransfer(EEPROM_I2C_BUS);
                                            for(;;){
                                                if( StartTransfer(FALSE,EEPROM_I2C_BUS) ){
                                                    if (TransmitOneByte(SlaveAddress.byte,EEPROM_I2C_BUS)){
                                                        if(I2CByteWasAcknowledged(EEPROM_I2C_BUS))break;else StopTransfer(EEPROM_I2C_BUS);
                                                    }
                                                }
                                            }
                                            StopTransfer(EEPROM_I2C_BUS);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    EE_WC_High();
}

//**************************************************************
//***  lettura byte da eeprom             **********************
//**************************************************************
void WriteWordEE(word add,word val){
    byte s[2];
	s[0]=(byte)(val>>8);
	s[1]=(byte)(val&0xff);
	WritePageEE(add,s,2);    
}

//**************************************************************
//*** scrive pagina di pagelength byte in eeprom   *************
//**************************************************************
void WritePageEE(word add,byte * pbuf,byte pagelength){
    unsigned char i=0;

    for(i=0;i<pagelength;i++){
        WriteByteEE(add+i,pbuf[i]);
    }
}

	

//**************************************************************
//*** scrive pagina di pagelength byte in eeprom   *************
//**************************************************************
void refreshEE(void){
	word eeadd;

//	EECON1bits.CFGS=0;
//	EECON1bits.EEPGD=0;
//	INTCONbits.GIE=0;
//	EECON1bits.WREN=1;
//			
//	for(eeadd=0;eeadd<1024;eeadd++){
//		EEADRH=(byte)(eeadd>>8);
//		EEADR=(byte)(eeadd&0xff);
//		EECON1bits.RD=1;
//		EECON2=0x55;
//		EECON2=0xaa;
//		EECON1bits.WR=1;
//		while(EECON1bits.WR);
//		}
//	EECON1bits.WREN=0;
//	INTCONbits.GIE=1;
}


//**************************************************************
//***  lettura byte da eeprom             **********************
//**************************************************************
byte readByteEE(word add){
unsigned char i2cbyte;
    unsigned char EepromAddress;
    I2C_7_BIT_ADDRESS   SlaveAddress;

    if(add&0x8000){
        EepromAddress=EEPROM1_ADDRESS;
        add&=(~0x8000);
    }else{
        EepromAddress=EEPROM0_ADDRESS;
    }
    for(;;){
        I2C_FORMAT_7_BIT_ADDRESS(SlaveAddress, EepromAddress, I2C_WRITE);
        if( StartTransfer(FALSE,EEPROM_I2C_BUS) ){
            if (TransmitOneByte(SlaveAddress.byte,EEPROM_I2C_BUS)){
                if(I2CByteWasAcknowledged(EEPROM_I2C_BUS)){
                    if (TransmitOneByte((unsigned char)(add>>8),EEPROM_I2C_BUS)){
                        if(I2CByteWasAcknowledged(EEPROM_I2C_BUS)){
                            if (TransmitOneByte((unsigned char)(add&0xff),EEPROM_I2C_BUS)){
                                if(I2CByteWasAcknowledged(EEPROM_I2C_BUS)){
                                    if( StartTransfer(TRUE,EEPROM_I2C_BUS) ){
                                        I2C_FORMAT_7_BIT_ADDRESS(SlaveAddress, EepromAddress, I2C_READ);
                                            if (TransmitOneByte(SlaveAddress.byte,EEPROM_I2C_BUS)){
                                                if(I2CByteWasAcknowledged(EEPROM_I2C_BUS)){
                                                    if(I2CReceiverEnable(EEPROM_I2C_BUS, TRUE) != I2C_RECEIVE_OVERFLOW){
                                                        while(!I2CReceivedDataIsAvailable(EEPROM_I2C_BUS));
                                                        i2cbyte = I2CGetByte(EEPROM_I2C_BUS);
                                                        StopTransfer(EEPROM_I2C_BUS);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
    }
    return(i2cbyte);
}


//**************************************************************
//***  lettura Dword da eeprom             *********************
//**************************************************************
dword readDWordEE(word add){
    dword dw;
    byte temp;
    
    temp=readByteEE(add);	
	dw=(dword)temp<<24;
	temp=readByteEE(add+1);	
	dw|=(dword)temp<<16;
	temp=readByteEE(add+2);	
	dw|=(dword)temp<<8;
	temp=readByteEE(add+3);	
	dw|=(dword)temp;
	
    return dw;
}


//**************************************************************
//***  lettura word da eeprom             **********************
//**************************************************************
word readWordEE(word add){
    word w;
    byte temp;
    
    temp=readByteEE(add);	
	w=(word)temp<<8;
	temp=readByteEE(add+1);	
	w|=(word)temp;
	
    return w;
}



//*************************************************
//*** lettura paramentri       ********************
//*************************************************
//void readPar(void){
//    
//    Ptara=readWordEE(WE_RE_T);
//    
//    PtaraOlioIn=readWordEE(WE_OI_T);
//    PtaraOlioOut=readWordEE(WE_DO_T);
//    PtaraUV=readWordEE(WE_UV_T);
//    PtaraOlioInSpecial=readWordEE(WE_OI_S);	   
//    PtaraUVSpecial=readWordEE(WE_UV_S);	   
//
//    PmaxRel=readWordEE(WE_RE_R);
//    PmaxAbs=readWordEE(WE_RE_M);
//    
//    thlo=readWordEE(TH_LO);
//    thhi=readWordEE(TH_HI);
//    thvv=readWordEE(TH_VV);
//    thlt=readWordEE(TH_LT);
//    thio=readWordEE(TH_IO);
//    thhs=readWordEE(TH_HS);
//    
//    offsetVD=readWordEE(OF_PR);;
//
//    lingua=readByteEE(ADDLINGUA);
//    if (lingua>19)lingua=0;		 
//    
//    zero[SEL_GAS]=readDWordEE(ADDZERO+(SEL_GAS*4));
//    zero[SEL_PT]=readDWordEE(ADDZERO+(SEL_PT*4));
//    
//    kConv[SEL_GAS]=readWordEE(ADDK+(2*SEL_GAS));
//    kConv[SEL_PT]=readWordEE(ADDK+(2*SEL_PT));
//    
//    pAlSoil=readWordEE(WE_DO_M);
//    
//    ltubi=readWordEE(LE_HO);
//    config=readWordEE(CONFIG);    
////    ckr=readWordEE(WE_TA_R);    
//    printrec=readWordEE(PA_LPT);    
//    fprint=readWordEE(PA_PRN);    
//    
//    pson=readWordEE(PSON);
//    thpon=readWordEE(THPON);
//    psk=readWordEE(PSK);
//    thpoff=readWordEE(THPOFF);
//    oftc1=readWordEE(OF_TC1)*10;
//    rec_comp=readWordEE(REC_COMP);
//    rec_add=readWordEE(REC_ADD);
//    tipa=readWordEE(TI_PA);
//    thvent=readWordEE(TH_VENT);
////    ganauto=readWordEE(GANAUTO);
//    uvpresente=readWordEE(UVPRESENTE);
//    ntpresente=readWordEE(NTPRESENTE);
//    ntpresente=0;
////    ganpresente=readWordEE(GANPRESENTE);
////    afrpresente=readWordEE(AFRPRESENTE);
//}


void readStringEE(byte *fname, byte len){
    int i;
    word initadd=FILENAME;
    
    for(i=0;i<len;i++){
        fname[i]=readByteEE(initadd+i);
    }
}

void writeStringEE(byte *fname, byte len){
    int i;
    word initadd=FILENAME;
    
    for(i=0;i<len;i++){
        WriteByteEE(initadd+i,fname[i]);
    }
}
