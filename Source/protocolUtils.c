
#include "GLOBAL.H"
#include "HWUART.H"
#include "protocolUtils.h"

//preambolo UART
#define PRE1    0x05
#define PRE2    0xA0

byte protSendCommand(byte * cmdbuf, byte nchartx,word timeout, byte* rplbuf, word ncharrx,byte forceExit){
    byte checksum=0;
    static byte cs1,cs2;
    word i;
    timeout/=10;
    for(;;){
        if(nchartx){
            set485tx();
            txCharUart1(PRE1);txCharUart1(PRE2);
            checksum=0;
            for(i=0;i<nchartx;i++){
                txCharUart1(cmdbuf[i]);
                checksum^=cmdbuf[i];
            }
            txCharUart1(checksum);
        }
        set485rx();
        if(ncharrx){
            checksum=0;
            for(i=0;i<ncharrx;i++){
                rplbuf[i]=rxb(timeout);
                checksum^=rplbuf[i];
                timeout=50/10;
            }
        }
        cs1=rxb(timeout);
        timeout=50/10;
        cs2=rxb(50/10);
        cs2=~cs2;
        if((cs1==cs2)&&(cs1==checksum))break;
        if(forceExit)return 0;
        PORTSetBits(LED2_PORTID,LED2_PIN);
        rt10ms1=2;
        while(rt10ms1);
        resetUart1();
        PORTClearBits(LED2_PORTID,LED2_PIN);
    }
    return 1;
}

//**************************************************************
//***  accende usb su scheda b                ******************
//************************************************************** 
void cmdUsbOn(void){
    byte cmdbuf[6];
    byte rplbuf[1];
    
    cmdbuf[0]='u';
    cmdbuf[1]='s';
    cmdbuf[2]='b';
    cmdbuf[3]='O';
    cmdbuf[4]='N';
    cmdbuf[5]=' ';
    (void)protSendCommand(cmdbuf, 6,500, rplbuf, 0,0);
}

//**************************************************************
//***  spegne usb su scheda b                 ******************
//************************************************************** 
void cmdUsbOff(void){
    byte cmdbuf[6];
    byte rplbuf[1];
    
    cmdbuf[0]='u';
    cmdbuf[1]='s';
    cmdbuf[2]='b';
    cmdbuf[3]='O';
    cmdbuf[4]='F';
    cmdbuf[5]='F';
    (void)protSendCommand(cmdbuf, 6,500, rplbuf, 0,0);
}


//**************************************************************
//***  legge stato usb su scheda b            ******************
//************************************************************** 
void cmdUsbSt(byte * st){
    byte cmdbuf[6];
    
    cmdbuf[0]='u';
    cmdbuf[1]='s';
    cmdbuf[2]='b';
    cmdbuf[3]='S';
    cmdbuf[4]='T';
    cmdbuf[5]=' ';
    (void)protSendCommand(cmdbuf, 6,50, st, 1,0);
}

//**************************************************************
//***  aprefile                               ******************
//************************************************************** 
byte cmdUsbOpenFile(char * filename){
    byte cmdbuf[13];
    byte rplbuf[2];
    byte b;
    
    cmdbuf[0]='A';
    for(b=0;b<12;b++)cmdbuf[b+1]=filename[b];
    (void)protSendCommand(cmdbuf, 13,2000, rplbuf, 2,0);
    if(rplbuf[0]=='A')return 0; else if(rplbuf[0]=='a')return rplbuf[1];
}

//**************************************************************
//***  scrive file                            ******************
//************************************************************** 
byte cmdUsbAppendFile(byte * buf, byte nchar){
    byte cmdbuf[255];
    byte rplbuf[2];
    byte b;
    
    cmdbuf[0]='B';
    cmdbuf[1]=nchar;
    for(b=0;b<nchar;b++)cmdbuf[b+2]=buf[b];
    (void)protSendCommand(cmdbuf, nchar+2,2000, rplbuf, 2,0);
    if(rplbuf[0]=='B')return 0;else if(rplbuf[0]=='b')return rplbuf[1];
}

//**************************************************************
//***  chiude file                            ******************
//************************************************************** 
void cmdUsbCloseFile(void){
    byte cmdbuf[6];
    byte rplbuf[1];
    
    cmdbuf[0]='u';
    cmdbuf[1]='s';
    cmdbuf[2]='b';
    cmdbuf[3]='C';
    cmdbuf[4]='F';
    cmdbuf[5]=' ';
    (void)protSendCommand(cmdbuf, 6,500, rplbuf, 1,0);
    
}

//****************************************************
//***  cmdreset   ************************************
//**************************************************** 
void cmdReset(void){
    byte cmdbuf[5];
    byte rplbuf[1];
    
    cmdbuf[0]='R';
    cmdbuf[1]='E';
    cmdbuf[2]='S';
    cmdbuf[3]='E';
    cmdbuf[4]='T';
    (void)protSendCommand(cmdbuf, 5,50, rplbuf, 0,0);
}

//****************************************************
//***  cmd check app valid ***************************
//**************************************************** 
void cmdCheckAppValid(byte * res){
    byte cmdbuf[4];
    byte rplbuf[1];
    
    cmdbuf[0]='X';
    cmdbuf[1]='C';
    cmdbuf[2]='A';
    cmdbuf[3]='V';
    (void)protSendCommand(cmdbuf, 4,50, rplbuf, 1,0);
    *res=rplbuf[0];
}

//**************************************************************
//***  aprefile in lettura                    ******************
//************************************************************** 
byte cmdUsbReadFile(char * filename){
    byte cmdbuf[13];
    byte rplbuf[2];
    byte b;
    
    cmdbuf[0]='a';
    for(b=0;b<12;b++)cmdbuf[b+1]=filename[b];
    (void)protSendCommand(cmdbuf, 13,2000, rplbuf, 2,0);
    if(rplbuf[0]=='A')return 0;
    else if(rplbuf[0]=='a')return rplbuf[1];
}

//**************************************************************
//***  aprefile in lettura                    ******************
//************************************************************** 
void cmdUsbSearchWow(char * filename){
    byte cmdbuf[1];
    byte b;
    
    cmdbuf[0]='D';
    (void)protSendCommand(cmdbuf, 1,2000, filename, 12,0);
}


//**************************************************************
//***  aprefile in lettura                    ******************
//************************************************************** 
byte cmdUsbCopyFile(char * filename){
    byte cmdbuf[13];
    byte rplbuf[1];
    byte b;
    
    cmdbuf[0]='c';
    for(b=0;b<12;b++)cmdbuf[b+1]=filename[b];
    (void)protSendCommand(cmdbuf, 13,2000, rplbuf, 1,0);
    return rplbuf[0];
}

//**************************************************************
//***  aprefile in lettura                    ******************
//************************************************************** 
byte cmdUsbWaitCopyFile(void){
    byte cmdbuf[1];
    byte rplbuf[1];
    
    cmdbuf[0]='d';
    (void)protSendCommand(cmdbuf, 1,2000, rplbuf, 1,0);
    return rplbuf[0];
}

//**************************************************************
//***  prog scheda b                          ******************
//************************************************************** 
void cmdUsbProgB(void){
    byte cmdbuf[4];
    byte rplbuf[1];
    
    cmdbuf[0]='X';
    cmdbuf[1]='P';
    cmdbuf[2]='S';
    cmdbuf[3]='B';
    (void)protSendCommand(cmdbuf, 4,10000, rplbuf, 0,0);
}

//**************************************************************
//***  jump app scheda b                          ******************
//************************************************************** 
void cmdUsbJumpAppB(void){
    byte cmdbuf[4];
    byte rplbuf[1];
    
    cmdbuf[0]='X';
    cmdbuf[1]='J';
    cmdbuf[2]='S';
    cmdbuf[3]='B';
    (void)protSendCommand(cmdbuf, 4,50, rplbuf, 0,0);
    
}

//****************************************************
//***  cmd check app valid ***************************
//**************************************************** 
void cmdCheckBootLoaderMode(byte * res){
    byte cmdbuf[4];
    byte rplbuf[1];
    
    cmdbuf[0]='X';
    cmdbuf[1]='B';
    cmdbuf[2]='L';
    cmdbuf[3]='M';
    (void)protSendCommand(cmdbuf, 4,50, rplbuf, 1,0);
    *res=rplbuf[0];
}

//**************************************************************
//***  manda i led e legge tastiera ed encoder *****************
//************************************************************** 
void setLedsReadKeyNtc(unsigned char bz,byte leds,byte lcdval,byte * pkey, char * pencoder,word * pntc){
    byte cmdbuf[4];
    byte rplbuf[4];
    
    cmdbuf[0]='T';
    cmdbuf[1]=bz;
    cmdbuf[2]=leds;
    cmdbuf[3]=lcdval;
    (void)protSendCommand(cmdbuf, 4,100, rplbuf, 4,0);
    *pkey=rplbuf[0];
    *pencoder+=rplbuf[1];
    *pntc=rplbuf[2];
    *pntc<<=8;
    *pntc|=rplbuf[3];
}

void readClock(byte *g,byte*m,byte*a,byte*h,byte*mi,byte*s){
    byte cmdbuf[1];
    byte rplbuf[7];
    
    cmdbuf[0]='W';
    (void)protSendCommand(cmdbuf, 1,100, rplbuf, 7,0);

    *g=rplbuf[0];
    *m=rplbuf[1];
    *a=rplbuf[2];
    *h=rplbuf[3];
    *mi=rplbuf[4];
    *s=rplbuf[5];
}

void writeClock(byte *g,byte*m,byte*a,byte*h,byte*mi,byte*s){
    byte cmdbuf[8];
    byte rplbuf[1];
    
    cmdbuf[0]='w';
    cmdbuf[1]=*g;
    cmdbuf[2]=*m;
    cmdbuf[3]=*a;
    cmdbuf[4]=*h;
    cmdbuf[5]=*mi;
    cmdbuf[6]=*s;
    cmdbuf[7]=0;
    (void)protSendCommand(cmdbuf, 8,100, rplbuf, 0,0);
}

void readFlash(byte * bufadd,dword flashadd,word nchar){
    byte cmdbuf[7];
    
    cmdbuf[0]='M';
    cmdbuf[1]=(flashadd>>24)&0xff;
    cmdbuf[2]=(flashadd>>16)&0xff;
    cmdbuf[3]=(flashadd>>8)&0xff;
    cmdbuf[4]=flashadd&0xff;
    cmdbuf[5]=(nchar>>8)&0xff;
    cmdbuf[6]=nchar&0xff;
    (void)protSendCommand(cmdbuf, 7,100, bufadd, nchar,0);
}


//****************************************************
//***  legge versione scheda b **********************
//**************************************************** 
byte cmdReadBVers(byte * hw,byte * sw){
    byte cmdbuf[4];
    byte rplbuf[2]={0,0};
    byte res;
    byte i;
    
    cmdbuf[0]='V';
    for(i=0;i<20;i++){
        res=protSendCommand(cmdbuf, 1,50, rplbuf, 2,1);
        if(res)break;
    }
    
    *hw=rplbuf[0];
    *sw=rplbuf[1];
    return res;
}