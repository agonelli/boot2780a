
#define _DECL_LCD20X4
#include "global.h"
#include "Lcd20x4.h" 

#include "HWGPIO.h"
#include "hwtimer.h"
#include "lcd20x4.h"


extern byte bcdToByte(byte b);
extern void wait10ms(byte t);

const char tohex[]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
//**************************************************************
//***  Print hex LCD *****************************************
//**************************************************************
void printhexlcd(byte add,dword dw){
	char s[9];
	
    s[0]=tohex[(dw>>28)&0xf];
    s[1]=tohex[(dw>>24)&0xf];
    s[2]=tohex[(dw>>20)&0xf];
    s[3]=tohex[(dw>>16)&0xf];
    s[4]=tohex[(dw>>12)&0xf];
    s[5]=tohex[(dw>>8)&0xf];
    s[6]=tohex[(dw>>4)&0xf];
    s[7]=tohex[dw&0xf];
    s[8]=0;
        
	printSlcd(add,s);
}



//**************************************************************
//***  Print dword LCD *****************************************
//**************************************************************
void printdwlcd(byte add,dword dw,byte flags){
    char s[11];
	byte f=0;
    byte i;
    byte temp;
	byte nchar;

	nchar=flags&0xf;
	flags&=(~0x0f);	
	if(dw&0x80000000){
		if(flags&NEGATIVO){
			f|=NEGATIVO;
			dw=0xffffffff-dw+1;
        }
    }
	for(i=0;i<11;i++)s[i]=0;

	s[0]=(char)(dw/1000000000)+'0';			//converte in digit
	dw=dw%1000000000;
	s[1]=(char)(dw/100000000)+'0';
	dw=dw%100000000;
	s[2]=(char)(dw/10000000)+'0';
	dw=dw%10000000;
	s[3]=(char)(dw/1000000)+'0';
	dw=dw%1000000;
	s[4]=(char)(dw/100000)+'0';
	dw=dw%100000;
	s[5]=(char)(dw/10000)+'0';
	dw=dw%10000;
	s[6]=(char)(dw/1000)+'0';
	dw=dw%1000;
    s[7]=(char)(dw/100)+'0';
	dw=dw%100;
    s[8]=(char)(dw/10)+'0';
	s[9]=(dw%10+'0');

	if(nchar==9){
		if(flags&ALIGNDX){for(i=0;i<9;i++)s[i]=s[i+1];}
	}else if(nchar==8){
		if(flags&ALIGNDX){for(i=0;i<8;i++)s[i]=s[i+2];}
	}else if(nchar==7){
		if(flags&ALIGNDX){for(i=0;i<7;i++)s[i]=s[i+3];}
	}else if(nchar==6){
		if(flags&ALIGNDX){for(i=0;i<6;i++)s[i]=s[i+4];}
	}else if(nchar==5){
		if(flags&ALIGNDX){for(i=0;i<5;i++)s[i]=s[i+5];}
	}else if(nchar==4){
		if(flags&ALIGNDX){for(i=0;i<4;i++)s[i]=s[i+6];}
    }

/*	if(flags&TRIMZERO){
		for(i=0;i<(nchar-1);i++){
			if(s[i]!='0')break;
        }
		for(temp=0;i<nchar;i++){
			s[temp]=s[i];
            temp++;
        }
        //curidx=temp;
		for(;temp<nchar;temp++){
			s[temp]=' ';
        }
		}	*/	
	if(flags&NOZERO){
		for(i=0;i<(nchar-1);i++){
			if(s[i]=='0')s[i]=' ';else break;
    }
            }
	if(f&NEGATIVO)s[0]='-';
	s[nchar]=0;
	if(flags&TOPRN){
		sendSprn(s,nchar);
        }
	else printSlcd(add,s);
    }


//**************************************************************
//***  Set Symbol  *********************************************
//**************************************************************
//void setSymbolLCD(void){
//	byte i;
//	byte j;
//	byte c;
//	dword add;

//	waitbflcd();
//	writelcd(0,0x40);
//	waitbflcd();
//	for(j=0;j<8;j++){
//		for(i=0;i<8;i++){
//		    add=STARTSYMBOL+(dword)i+(dword)(j*8);
//			readFlash(&c,add,1);
//		    waitbflcd();
//			writelcd(1,c);
//		}
//	}
//}


//**************************************************************
//***  clr lcd  ************************************
//**************************************** 
void clrlcd(void){
    byte cmdbuf[1];
    byte rplbuf[1];
    
    cmdbuf[0]='C';
    protSendCommand(cmdbuf, 1,50, rplbuf, 0);
}


//**************************************************************
//***  Print LCD  **********************************************
//**************************************************************
void printClcd(byte add,const  char * p){
    byte cmdbuf[23];
    byte rplbuf[1];
	byte i;
    
    for(i=0;i<20;i++){
        if(p[i]==0)break;
        else cmdbuf[i+3]=p[i];
    }
    if(i==0)return;
    
    cmdbuf[0]='P';
    cmdbuf[1]=add;
    cmdbuf[2]=i;
    protSendCommand(cmdbuf, i+3,50, rplbuf, 0);
}

//****************************************
//***  stringa da flash  a lcd ***********
//**************************************** 
void printFlashLCD(word add,byte lcdadd,byte nchar){
    byte cmdbuf[6];
    byte rplbuf[1];

    cmdbuf[0]='F';
    cmdbuf[1]=(byte)(add>>8);
    cmdbuf[2]=(byte)(add&0xff);
    cmdbuf[3]=lcdadd;
    cmdbuf[4]=nchar;
    cmdbuf[5]=lingua;
    protSendCommand(cmdbuf,6,50, rplbuf, 0);

}



//**************************************************************
//***  Print LCD  **********************************************
//**************************************************************
void printSlcd(byte add,char * p){
    byte cmdbuf[23];
    byte rplbuf[1];
    byte i;
    
    for(i=0;i<20;i++){
        if(p[i]==0)break;
        else cmdbuf[i+3]=p[i];
}
    if(i==0)return;

    cmdbuf[0]='P';
    cmdbuf[1]=add;
    cmdbuf[2]=i;
    protSendCommand(cmdbuf, i+3,50, rplbuf, 0);
 }


//**************************************************************
//***  Print word LCD ******************************************
//************************************************************** 
void printwlcd(byte add,word w,byte flags){
    char s[7];
    byte f=0;
    byte i;
    //byte temp;
    dword dw;
	byte nchar;
	
	nchar=flags&0x7;
	flags&=(~0x07);	
    if(w&0x8000){
	if(flags&NEGATIVO){
            f|=NEGATIVO;
            w=0xffff-w+1;
} 
    }
    dw=(dword)w+((dword)(flags&3)<<16);
    s[5]=0;
    s[6]=0;

    s[0]=(char)(dw/10000)+'0';			//converte in digit
    dw=dw%10000;
    s[1]=(char)(dw/1000)+'0';
    dw=dw%1000;
    s[2]=(char)(dw/100)+'0';
    dw=dw%100;
    s[3]=(char)(dw/10)+'0';
    s[4]=(dw%10+'0');
    if(nchar==4){
	if(flags&ALIGNDX){for(i=0;i<4;i++)s[i]=s[i+1];}
	s[4]=0;
    }else if(nchar==3){
	if(flags&ALIGNDX){for(i=0;i<3;i++)s[i]=s[i+2];}
	s[3]=0;
    }else if(nchar==2){
	if(flags&ALIGNDX){for(i=0;i<2;i++)s[i]=s[i+3];}
	s[2]=0;
    }else if(nchar==1){
	if(flags&ALIGNDX){for(i=0;i<1;i++)s[i]=s[i+4];}
	s[1]=0;
    }
/*    if(flags&TRIMZERO){
	for(i=0;i<(nchar-1);i++){
            if(s[i]!='0')break;
	}
	for(temp=0;i<nchar;i++){
            s[temp]=s[i];
            temp++;
	}
		//curidx=temp;
	for(;temp<nchar;temp++){
            s[temp]=' ';
	}
    }*/
    if(flags&NOZERO){
	for(i=0;i<(nchar-1);i++){
            if(s[i]=='0')s[i]=' ';else break;
	}
    }
    if(f&NEGATIVO)s[0]='-';
/*    if(flags&KG){
	s[5]=s[4];
	s[4]=s[3];
	s[3]=s[2];
	s[2]='.';
	if(s[5]==' ')s[5]='0';
	if(s[4]==' ')s[4]='0';
	if(s[3]==' ')s[3]='0';
	if(s[1]==' ')s[1]='0';
    }*/
    printSlcd(add,s);
}

//**************************************************************
//***  barra di posizione menu *********************************
//************************************************************** 
void bar(byte min,byte max,byte val){
    byte i;
    byte s[2];
    s[1]=0;
    s[0]=1;
    if(val>(max-min))val=max-min;
    if(val>0){
	for(i=0;i<val;i++)printSlcd(min+i,s);//3
    }
    s[0]=2;
    printSlcd(min+val,s);
    s[0]=1;
    if(val<(max-min)){
    	for(i=val+1;i<(max-min);i++)printSlcd(min+i,s);
    }
}

//**************************************************************
//***  lampeggio cursore       *********************************
//************************************************************** 
void setCursor(byte add,byte flag){
    byte cmdbuf[3];
    byte rplbuf[1];
    
    cmdbuf[0]='U';
    cmdbuf[1]=add;
    cmdbuf[2]=flag;
    protSendCommand(cmdbuf,3,50, rplbuf, 0);
    
    
}
