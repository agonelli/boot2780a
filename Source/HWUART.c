#include "xc.h"
#include"peripheral\uart.h"
//
#include "global.H"
#include "HWUART.H"
//#include <plib.h>
#include "Hardwareprofile.H"
#include "protocolUtils.h"

#define bps_UART_485 57600//38400//19200
#define bps_UART_PRN 115200
byte RdRS232(void);

//********************************************************************************************
//***************************** UART1 --> CAP ************************************************
//********************************************************************************************
void initUart1(void){
    RPD3Rbits.RPD3R=0x03;//rpd3
    U1RXRbits.U1RXR=0x00;//rpd2
    UARTConfigure(UART1, UART_ENABLE_PINS_TX_RX_ONLY);
    UARTSetFifoMode(UART1, UART_INTERRUPT_ON_TX_NOT_FULL | UART_INTERRUPT_ON_RX_NOT_EMPTY);
    UARTSetLineControl(UART1, UART_DATA_SIZE_8_BITS | UART_PARITY_NONE | UART_STOP_BITS_1);
    UARTSetDataRate(UART1, GetPeripheralClock(), bps_UART_485);
    UARTEnable(UART1, UART_ENABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));
    set485rx();

}

void txCharUart1(unsigned char b){
    while(!UARTTransmitterIsReady(UART1));
    UARTSendDataByte(UART1, b);
    while(!UARTTransmitterIsReady(UART1));
}


unsigned char rxCharUart1(unsigned char * b){
    if(UARTReceivedDataIsAvailable ( UART1 )){
        *b=UARTGetDataByte ( UART1);
        return 1;
    }else return 0;
}

byte rxb(unsigned short timeout){
    byte res=0;
    UART_LINE_STATUS st;
    timeoutUart=timeout;
    for(;;){
        if(timeoutUart==0)break;
        if(rxCharUart1(&res))break;
        st= UARTGetLineStatus ( UART1 );        
        if((st&UART_FRAMING_ERROR)||(st&UART_PARITY_ERROR)||(st&UART_OVERRUN_ERROR)){
            UARTEnable(UART1, UART_DISABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));
            UARTEnable(UART1, UART_ENABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));
        }
    }
    return res;
}

unsigned char dataReadyUart1(void){
    if(UARTReceivedDataIsAvailable ( UART1 ))return 1;else return 0;
}
unsigned char readCharUart1(void){
    return UARTGetDataByte ( UART1);
}

void set485tx(void){
    SysDelayUs(50);
    PORTSetBits(DIR485_PORTID, DIR485_PIN);
}

void set485rx(void){
    while(!UARTTransmissionHasCompleted(UART1));
    //SysDelayUs(260);
    PORTClearBits(DIR485_PORTID, DIR485_PIN);
}

//************ UART3 --> stampante    *****************************
void initUart3(void){
    RPB9Rbits.RPB9R=0x01;//rpB9
    //U4RXRbits.U4RXR=0x02;//rpf4
    UARTConfigure(UART3, UART_ENABLE_PINS_TX_RX_ONLY);
    UARTSetFifoMode(UART3, UART_INTERRUPT_ON_TX_NOT_FULL | UART_INTERRUPT_ON_RX_NOT_EMPTY);
    UARTSetLineControl(UART3, UART_DATA_SIZE_8_BITS | UART_PARITY_NONE | UART_STOP_BITS_1);
    UARTSetDataRate(UART3, GetPeripheralClock(), 115200);
    UARTEnable(UART3, UART_ENABLE_FLAGS(UART_PERIPHERAL |  UART_TX));
//    PRN_RESET_High();
}

void txCharUart3(unsigned char b){
    while(!UARTTransmitterIsReady(UART3));
    UARTSendDataByte(UART3, b);
}

void txStringUart3(const unsigned char * b){
    unsigned char i;
    unsigned char idx=0;

    for(i=0;i<0xff;i++){
        if(b[i]==0)break;
    }
    while(idx<i){
        txCharUart3(b[idx]);
        idx++;
    }
}

////************ UART2 --> gas analyzer *****************************
//void initUart4(void){
//    RPB8Rbits.RPB8R=0x02;//rpB8
//    //U4RXRbits.U4RXR=0x02;//rpf4
//    UARTConfigure(UART3, UART_ENABLE_PINS_TX_RX_ONLY);
//    UARTSetFifoMode(UART3, UART_INTERRUPT_ON_TX_NOT_FULL | UART_INTERRUPT_ON_RX_NOT_EMPTY);
//    UARTSetLineControl(UART3, UART_DATA_SIZE_8_BITS | UART_PARITY_NONE | UART_STOP_BITS_1);
//    UARTSetDataRate(UART3, GetPeripheralClock(), 9600);
//    UARTEnable(UART3, UART_ENABLE_FLAGS(UART_PERIPHERAL |  UART_TX));
////    PRN_RESET_High();
//}
//
////************ UART2 --> stampante    *****************************
//void initUart2(void){
//    U2RXRbits.U2RXR=0x04;   //rx su pf1
//    RPF0Rbits.RPF0R=1;      //tx su pf0
//    //RPB9Rbits.RPB9R=0x01;//rpf1
//    //U4RXRbits.U4RXR=0x02;//rpf4
//    UARTConfigure(UART2, UART_ENABLE_PINS_TX_RX_ONLY);
//    UARTSetFifoMode(UART2, UART_INTERRUPT_ON_TX_NOT_FULL | UART_INTERRUPT_ON_RX_NOT_EMPTY);
//    UARTSetLineControl(UART2, UART_DATA_SIZE_8_BITS | UART_PARITY_NONE | UART_STOP_BITS_1);
//    UARTSetDataRate(UART2, GetPeripheralClock(), bps_UART_PRN);
//    UARTEnable(UART2, UART_ENABLE_FLAGS(UART_PERIPHERAL |  UART_TX));
////    PRN_RESET_High();
//}
//
//void txCharUart2(unsigned char b){
//    while(!UARTTransmitterIsReady(UART2));
//    UARTSendDataByte(UART2, b);
//}





void resetUart1(void){
    UARTEnable(UART1, UART_DISABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));
    UARTEnable(UART1, UART_ENABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));
}
        
void resetUart3(void){
    UARTEnable(UART3, UART_DISABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));
    UARTEnable(UART3, UART_ENABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));
}

