/********************************************************************
FileName:     usb_host_bootloader.c
Dependencies: See INCLUDES section
Processor:		PIC32 USB MSD Microcontrollers
Hardware:		
Complier:  	Microchip C18 (for PIC18), C30 (for PIC24), C32 (for PIC32)
Company:		Microchip Technology, Inc.
*******************************************************************/
#define GLOBAL
#include "global.h"
#include "MDD File System/FSIO.h"
#include "NVMem.h"

#include "Bootloader.h"
#include <plib.h>
#include "HardwareProfile.h"

#include "HWGPIO.h"
#include "HWTIMER.h"
#include "protocolUtils.h"
#include "GenericTypeDefs.h"
#include "ee.h"

// *****************************************************************************
// *****************************************************************************
// *****************************************************************************
// *****************************************************************************
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
// *****************************************************************************
// Device Configuration Bits (Runs from Aux Flash)
// *****************************************************************************
#ifdef OMIT_CONFIGURATIONS
#else
#   pragma config UPLLEN   = ON            // USB PLL Enabled
#   pragma config FPLLMUL  = MUL_20        // PLL Multiplier
#   pragma config UPLLIDIV = DIV_2         // USB PLL Input Divider
#   pragma config FPLLIDIV = DIV_4//DIV_2         // PLL Input Divider
#   pragma config FPLLODIV = DIV_1//DIV_2         // PLL Output Divider
#   pragma config FPBDIV   = DIV_1         // Peripheral Clock divisor
#   pragma config FWDTEN   = OFF           // Watchdog Timer
#   pragma config WDTPS    = PS1           // Watchdog Timer Postscale
                             //#pragma config FCKSM    = CSDCMD        // Clock Switching & Fail Safe Clock Monitor
#   pragma config OSCIOFNC = OFF           // CLKO Enable
#   pragma config POSCMOD  = HS            // Primary Oscillator
#   pragma config IESO     = OFF           // Internal/External Switch-over
#   pragma config FSOSCEN  = OFF           // Secondary Oscillator Enable (KLO was off)
#   pragma config FNOSC    = PRIPLL        // Oscillator Selection
#   pragma config CP       = OFF           // Code Protect
#   pragma config BWP      = OFF           // Boot Flash Write Protect
//   #pragma config PWP      = OFF           // Program Flash Write Protect
#   pragma config ICESEL   = ICS_PGx1      // ICE/ICD Comm Channel Select


// *****************************************************************************
// DEVCFG3
// USERID = No Setting
#   pragma config PMDL1WAY = OFF           // Peripheral Module Disable Configuration (Allow multiple reconfigurations)
#   pragma config IOL1WAY = OFF            // Peripheral Pin Select Configuration (Allow multiple reconfigurations)
#   pragma config FUSBIDIO = OFF           // USB USID Selection (Controlled by Port Function)
#   pragma config FVBUSONIO = OFF          // USB VBUS ON Selection (Controlled by Port Function)

#   if defined(TRANSPORT_LAYER_ETH)
#       pragma config FMIIEN = OFF, FETHIO = OFF	// external PHY in RMII/alternate configuration	
#   endif

#   if defined(__PIC32MX1XX_2XX__)
#   elif defined(__PIC32MX3XX_7XX__)
                             // For PIC32MX3xx, PIC32MX4xx, PIC32MX5xx, PIC32MX6xx and PIC32MX7xx 
                             // devices the ICE connection is on PGx2. .
#       pragma config ICESEL = ICS_PGx2    // ICE pins configured on PGx2, Boot write protect OFF.
                             //For PIC32MX3xx, PIC32MX4xx, PIC32MX5xx, PIC32MX6xx and PIC32MX7xx devices, 
                             //the output divisor is set to 1 to produce max 80MHz clock.
#       pragma config FPLLODIV = DIV_1         // PLL Output Divider: Divide by 1
#   endif
#endif

/******************************************************************************
Macros used in this file
*******************************************************************************/
#define SWITCH_PRESSED 0
#define DEV_CONFIG_REG_BASE_ADDRESS 0x9FC02FF0
#define DEV_CONFIG_REG_END_ADDRESS   0x9FC02FFF

#define DATA_RECORD 		0
#define END_OF_FILE_RECORD 	1
#define EXT_SEG_ADRS_RECORD 2
#define EXT_LIN_ADRS_RECORD 4

//#define KC		0x01
//#define KUP		0x02
//#define KSTARTSTOP	0x04
//#define KDOWN	0x08
//#define KMENU	0x10
//#define KV		0x20
//#define KR		0x40
//#define KNOKEY  0x0
//#define ALLKEYS 0x7f

#define REC_FLASHED 0
#define REC_NOT_FOUND 1
#define REC_FOUND_BUT_NOT_FLASHED 2

/************************************
type defs
**************************************/
typedef struct
{
    UINT8 *start;
    UINT8 len;
    UINT8 status;
}T_REC;

typedef struct 
{
UINT8 RecDataLen;
DWORD_VAL Address;
UINT8 RecType;
UINT8* Data;
UINT8 CheckSum;	
DWORD_VAL ExtSegAddress;
DWORD_VAL ExtLinAddress;
}T_HEX_RECORD;	

#ifdef __cplusplus
extern "C" {
#endif
void ConverAsciiToHex(UINT8* asciiRec, UINT8* hexRec);
    void EraseFlash(void);
    int WriteHexRecord2Flash(UINT8* HexRecord);
    void GestCom(void);
#ifdef __cplpusplus
}
#endif

/******************************************************************************
Global Variables
*******************************************************************************/
FSFILE * myFile;
size_t numBytes;
UINT pointer = 0;
UINT readBytes;
byte gwaitresponse=0;
byte gProgramFileEof=0;

UINT8 asciiBuffer[1024];
UINT8 asciiRec[200];
UINT8 hexRec[100];
T_REC record;
char gNameFile[12];
byte HWVersB;
byte SWVersB;

/****************************************************************************
Function prototypes
*****************************************************************************/
void ConvertAsciiToHex(UINT8* asciiRec, UINT8* hexRec);
void JumpToApp(void);
BOOL ValidAppPresent(void);
extern void sendCommand(unsigned char ID, unsigned short txsize,unsigned short rxsize);
extern void readStringEE(byte *fname, byte len);
extern void writeStringEE(byte *fname, byte len);




/********************************************************************
 * Function: 	main()
 * Overview: 	Main entry function. If there is a trigger or
 *				if there is no valid application, the device
 *				stays in firmware upgrade mode.
 ********************************************************************/
int main(void) {
    volatile UINT i;
    int value;
    byte appApresente;
    byte appBpresente;
    byte flagboot;
    byte contview=0;
    byte isProgramEnd=0;
    unsigned short st,memst;
    unsigned short timeout;
    unsigned char tst;
    unsigned char filefound;
    byte filename[15];
    byte foundfilename[15];
    word v,w;
    T_REC lrecord;
    UINT lpointer = 0;
    unsigned long AddFlash;
    unsigned long addFileFWA;
    unsigned short r;
    byte usbst;
    byte res;
    
    leds=0;
    // Setup configuration
    DDPCONbits.JTAGEN = 0;
    (void) SYSTEMConfig(SYS_FREQ, SYS_CFG_WAIT_STATES | SYS_CFG_PCACHE);
    value = SYSTEMConfigWaitStatesAndPB(GetSystemClock());
    // Enable the cache for the best performance
    CheKseg0CacheOn();
    value = OSCCON;
    while (!(value & 0x00000020)) {
        value = OSCCON; // Wait for PLL lock to stabilize
    }
    INTEnableSystemMultiVectoredInt();
    initGPIO(); //inizializza I/O
    initUart1();
    initUart3();
    //******** inizializza bus I2c *******************************************************
    tempdw0 = I2CSetFrequency(EEPROM_I2C_BUS, GetPeripheralClock(), I2C_CLOCK_FREQ);
    I2CEnable(EEPROM_I2C_BUS, TRUE);

    Timer1Init();
    INTEnableInterrupts(); 

    //RITARDO NECESSARIO rispetto alla partenza SLAVE
    rt10ms2=100;
    while(rt10ms2)
    clrlcd();
    setLedsReadKeyNtc(buz,leds,contrastolcd,&tst,&encoder,&adcntc2);
    (void)cmdReadBVers(&HWVersB,&SWVersB);
    appApresente=ValidAppPresent();
    flagboot=readByteEE(FLAGBOOT);
    if((flagboot==2)&&(tst==0))st=PROGRAM2770B;else st=CHECKAPPBVALID;
    if(tst==8){ //tasto start KV
        printClcd(RIGA4,"Boot v1.1");
        while(tst==8){
            setLedsReadKeyNtc(buz,leds,contrastolcd,&tst,&encoder,&adcntc2);
        }
    }
    cmdUsbOn();
    while(1){
//        if(st!=prevst){
//            prevst=st;
//        }
//            txCharUart3(' ');
//            txCharUart3(st);
//            txCharUart3(usbst);
        switch(st){
            case CHECKAPPBVALID:
                cmdCheckAppValid(&appBpresente);
                setLedsReadKeyNtc(buz,leds,contrastolcd,&tst,&encoder,&adcntc2);
                if(tst==KSU){
                }
                if((tst==0x44)||(appBpresente==0)||(appApresente==0)){ //KX KI
                    filename[0]=0xff;
                    writeStringEE(filename,12);
                }
                st=CHECKUSBPRESENT;
                rt10ms2=400;//3 secondi
                break;
            case CHECKUSBPRESENT:
                setLedsReadKeyNtc(buz,leds,contrastolcd,&tst,&encoder,&adcntc2);
                //Attendo che lo slave aggiorni il suo stato
                cmdUsbSt(&usbst);
                switch(usbst){
                    case ST_USB_NO_PRESENT:
                    case ST_USB_PRESENT:
                        //Salta all'applicazione se appA e appB sono valide AND NESSUN tasto premuto
                        if(((appApresente!=0)&&(appBpresente!=0))&&(tst==0)&&((flagboot==3)||(flagboot==0xff))){
                            if(rt10ms2==0)st=JUMPTOAPPB;
                        }else{
                            //clrlcd();
                            if(memst!=st){
                                memst=st;
                                printClcd(RIGA1,"Bootloader mode");
                                printClcd(RIGA2,"Please insert an usb");
                                printClcd(RIGA3,"memory containg an");
                                printClcd(RIGA4,"application file");
                            }
                            leds|=LCDON;
                            st=CHECKUSBPRESENT;
                        }
                        break;
                    case ST_USB_FS_OK:
                        clrlcd();                        
                        st=SEARCHFILE;
                        break;
                }                
                break;
            case SEARCHFILE:
                readStringEE(filename,12);
                //FileFound
                cmdUsbSearchWow(foundfilename);
                if(foundfilename[0]!=0xff){
                    //Copio il nome file trovato sul dongle in  gNameFile[]
                    filefound=0;
                    if(filename[0]==0xff)filefound=1;
                    else{
                        if((filename[0]==foundfilename[0])&&(filename[1]==foundfilename[1])&&(filename[2]==foundfilename[2])&&(filename[3]==foundfilename[3])){
                            w=foundfilename[4]-'0';
                            w=w*10;
                            w+=foundfilename[5]-'0';
                            w=w*10;
                            w+=foundfilename[6]-'0';
                            w=w*10;
                            w+=foundfilename[7]-'0';
                            v=filename[4]-'0';
                            v=v*10;
                            v+=filename[5]-'0';
                            v=v*10;
                            v+=filename[6]-'0';
                            v=v*10;
                            v+=filename[7]-'0';
                            if(w>v)filefound=1;
                        }
                    }
                    if(filefound){
                        memcpy(&gNameFile[0],foundfilename,12);
                        printClcd(RIGA1,"Search File ...");
                        printClcd(RIGA2,"Found");
                        gNameFile[12]=0;
                        printClcd(RIGA2+8,gNameFile);
                        st=COPYFILE;
                    }else if((appApresente!=0)&&(appBpresente!=0)){
                        st=JUMPTOAPPB;
                    }else{
                        //clrlcd();
                        if(memst!=st){
                            memst=st;
                            printClcd(RIGA1,"Bootloader mode");
                            printClcd(RIGA2,"Please insert an usb");
                            printClcd(RIGA3,"memory containg an");
                            printClcd(RIGA4,"application file");
                        }
                        st=CHECKUSBPRESENT;
                    }
                }else{
                    //FileNotFound
                    if((appApresente!=0)&&(appBpresente!=0))
                        st=JUMPTOAPPB;
                    else{
                        //clrlcd();
                        if(memst!=st){
                            memst=st;
                            printClcd(RIGA1,"Bootloader mode");
                            printClcd(RIGA2,"Please insert an usb");
                            printClcd(RIGA3,"memory containg an");
                            printClcd(RIGA4,"application file");
                        }
                        st=CHECKUSBPRESENT;
                    }
                }        
                break;
            case COPYFILE:
                filename[0]=0xff;
                writeStringEE(filename,12);
                flagboot=1;
                WriteByteEE(FLAGBOOT,flagboot);
                res=cmdUsbCopyFile(foundfilename);
                if(res!=255){
                    for(;;){
                        res=cmdUsbWaitCopyFile();
                        if(res==254){
                            writeStringEE(gNameFile,12);
                            flagboot=2;
                            WriteByteEE(FLAGBOOT,flagboot);
                            st=PROGRAM2770B;
                            break;
                        }else if(res==255){
                            st=CHECKAPPBVALID;
                            break;
                        }
                    }
                }else st=CHECKAPPBVALID;
                break;
            case PROGRAM2770B:
                clrlcd();
                printClcd(RIGA1,"UPDATE");
                printClcd(RIGA2,"STEP 1/2");
                cmdUsbProgB();
                st=PROGRAM2770A;
                break;
            case PROGRAM2770A:
                printClcd(RIGA3,"STEP 2/2");
                EraseFlash();
                lrecord.status = REC_NOT_FOUND;
                isProgramEnd   = 0;
                lpointer       = 0;
                if(HWVersB&WFLASMEM16M)addFileFWA=ADD_FILE_FWA_16M;else addFileFWA=ADD_FILE_FWA;
                for (AddFlash = addFileFWA; AddFlash < (addFileFWA+ADDFLASHEND);){
                    UINT8 lasciiBuffer[1024];
                    readFlash(&lasciiBuffer[lpointer],AddFlash,512);
                    contview++;
                    if(contview>=32){
                        contview=0;
#if 0
                        printhexlcd(RIGA4,AddFlash);
#endif
                    }
                    AddFlash += 512;
                    for(i = 0; i < (512 + lpointer); i++){
                        switch(lrecord.status){ //This state machine seperates-out the valid hex records from the read 512 bytes.
                            case REC_FLASHED:
                            case REC_NOT_FOUND:
                                if(lasciiBuffer[i] == ':'){ // We have a record found in the 512 bytes of data in the buffer.
                                    lrecord.start = &lasciiBuffer[i];
                                    lrecord.len = 0;
                                    lrecord.status = REC_FOUND_BUT_NOT_FLASHED;
                                }
                                break;
                            case REC_FOUND_BUT_NOT_FLASHED:
                                if((lasciiBuffer[i] == 0x0A) || (lasciiBuffer[i] == 0xFF)){
                                    // We have got a complete record. (0x0A is new line feed and 0xFF is End of file)
                                    // Start the hex conversion from element 1. This will discard the ':' which is the start of the hex record.
                                    ConvertAsciiToHex(&lrecord.start[1],hexRec);
                                    isProgramEnd=WriteHexRecord2Flash(hexRec);
                                    lrecord.status = REC_FLASHED;
                                }
                                break;
                        }
                        lrecord.len ++;         // Move to next byte in the buffer.
                    }
                    if(lrecord.status == REC_FOUND_BUT_NOT_FLASHED){
                        // We still have a half read record in the buffer. The next half part of the record is read when we read 512 bytes of data from the next file read. 
                        memcpy(lasciiBuffer, lrecord.start, lrecord.len);
                        lpointer = lrecord.len;
                        lrecord.status = REC_NOT_FOUND;
                    }else{
                        lpointer = 0;
                    }
                    switch(isProgramEnd){
                        case 0:
                            //Programmazione ancora in corso
                            break;
                        case 1:
                            //Programmazione conclusa con successo
                            flagboot=3;
                            WriteByteEE(FLAGBOOT,flagboot);
                            //printClcd(RIGA4,"OK SHUTDOWN THE UNIT");
                            //for(;;);
                            st=JUMPTOAPPB;
                            AddFlash=0xffffffff;
                            break;
                        case 2:
                            //Programmazione Errore
                            EraseFlash();
                            lrecord.status = REC_NOT_FOUND;
                            isProgramEnd   = 0;
                            lpointer = 0;
                            AddFlash = addFileFWA;
                            break;
                    }
                }
                
                break;
            case JUMPTOAPPB:
                cmdUsbJumpAppB();
                st=JUMPTOAPPA;
                break;
            case JUMPTOAPPA:
                cmdCheckBootLoaderMode(&res);
                if(res==0){
                    USBDisableInterrupts();
                    JumpToApp();
                }else cmdUsbJumpAppB();
                break;
        }
    }
    return 0;
}



/********************************************************************
* Function: 	JumpToApp()
* Overview: 	Jumps to application.
********************************************************************/
void JumpToApp(void){	
	void (*fptr)(void);
	fptr = (void (*)(void))USER_APP_RESET_ADDRESS;
	fptr();
}	

/********************************************************************
* Function: 	ConvertAsciiToHex()
* Input: 		Ascii buffer and hex buffer.
* Overview: 	Converts ASCII to Hex.
********************************************************************/
void ConvertAsciiToHex(UINT8* asciiRec, UINT8* hexRec){
	UINT8 i = 0;
	UINT8 k = 0;
	UINT8 hex;
    
	while((asciiRec[i] >= 0x30) && (asciiRec[i] <= 0x66)){
		// Check if the ascci values are in alpha numeric range.
		if(asciiRec[i] < 0x3A){
			// Numerical reperesentation in ASCII found.
			hex = asciiRec[i] & 0x0F;
		}else{
			// Alphabetical value.
			hex = 0x09 + (asciiRec[i] & 0x0F);						
		}
		// Following logic converts 2 bytes of ASCII to 1 byte of hex.
		k = i%2;
		if(k){
			hexRec[i/2] |= hex;
		}else{
			hexRec[i/2] = (hex << 4) & 0xF0;
		}	
		i++;		
	}		
}
// Do not change this
//#define FLASH_PAGE_SIZE 0x1000
#define FLASH_PAGE_SIZE 0x400 // 1K
/********************************************************************
* Function: 	EraseFlash()
* Overview: 	Erases Flash (Block Erase).
********************************************************************/
void EraseFlash(void){
	void * pFlash;
    UINT result;
    INT i;
    pFlash = (void*)APP_FLASH_BASE_ADDRESS;	
    for( i = 0; i < ((APP_FLASH_END_ADDRESS - APP_FLASH_BASE_ADDRESS + 1)/FLASH_PAGE_SIZE); i++ ){
        result = NVMemErasePage( pFlash + (i*FLASH_PAGE_SIZE) );
        if(result != 0){
            printClcd(RIGA1,"ERROR Erasing  ");
            printClcd(RIGA2,"Memory         ");
            while(1);
        } 
    }			           	     
}

/********************************************************************
* Function: 	WriteHexRecord2Flash()
* Overview: 	Writes Hex Records to Flash.
********************************************************************/
int WriteHexRecord2Flash(UINT8* HexRecord){
	static T_HEX_RECORD HexRecordSt;
	UINT8 Checksum = 0;
	UINT8 i;
	UINT WrData;
	UINT RdData;
	void* ProgAddress;
	UINT result;
    
	HexRecordSt.RecDataLen = HexRecord[0];
	HexRecordSt.RecType = HexRecord[3];	
	HexRecordSt.Data = &HexRecord[4];	
	
	// Hex Record checksum check.
	for(i = 0; i < HexRecordSt.RecDataLen + 5; i++){
		Checksum += HexRecord[i];
	}	
	
    if(Checksum != 0){
	    //Error. Hex record Checksum mismatch.
	    //Indicate Error by switching ON all LEDs.
	    //Error();
	    // Do not proceed further.
	    //while(1);
        return 2; //gestire errore si reinizia la programmazione
	} 
	else{
		// Hex record checksum OK.
		switch(HexRecordSt.RecType){
			case DATA_RECORD:  //Record Type 00, data record.
				HexRecordSt.Address.byte.MB = 0;
                HexRecordSt.Address.byte.UB = 0;
                HexRecordSt.Address.byte.HB = HexRecord[1];
                HexRecordSt.Address.byte.LB = HexRecord[2];
                
                // Derive the address.
                HexRecordSt.Address.Val = HexRecordSt.Address.Val + HexRecordSt.ExtLinAddress.Val + HexRecordSt.ExtSegAddress.Val;
                
                while(HexRecordSt.RecDataLen) // Loop till all bytes are done.
                {											
                    // Convert the Physical address to Virtual address. 
                    ProgAddress = (void *)PA_TO_KVA0(HexRecordSt.Address.Val);
                    
                    // Make sure we are not writing boot area and device configuration bits.
                    if(((ProgAddress >= (void *)APP_FLASH_BASE_ADDRESS) && (ProgAddress <= (void *)APP_FLASH_END_ADDRESS))
                            && ((ProgAddress < (void*)DEV_CONFIG_REG_BASE_ADDRESS) || (ProgAddress > (void*)DEV_CONFIG_REG_END_ADDRESS)))
                    {
                        if(HexRecordSt.RecDataLen < 4){
                            
                            // Sometimes record data length will not be in multiples of 4. Appending 0xFF will make sure that..
                            // we don't write junk data in such cases.
                            WrData = 0xFFFFFFFF;
                            memcpy(&WrData, HexRecordSt.Data, HexRecordSt.RecDataLen);	
                        }
                        else{	
                            memcpy(&WrData, HexRecordSt.Data, 4);
                        }		
                        // Write the data into flash.	
                        result = NVMemWriteWord(ProgAddress, WrData);	
                        // Assert on error. This must be caught during debug phase.		
                        if(result != 0){
                            //while(1);
                            return 2; //gestire errore si reinizia la programmazione
                        }									
                    }	
                    
                    // Increment the address.
                    HexRecordSt.Address.Val += 4;
                    // Increment the data pointer.
                    HexRecordSt.Data += 4;
                    // Decrement data len.
                    if(HexRecordSt.RecDataLen > 3){
                        HexRecordSt.RecDataLen -= 4;
                    }	
                    else{
                        HexRecordSt.RecDataLen = 0;
                    }	
                }
                break;
                
			case EXT_SEG_ADRS_RECORD:  // Record Type 02, defines 4th to 19th bits of the data address.
			    HexRecordSt.ExtSegAddress.byte.MB = 0;
				HexRecordSt.ExtSegAddress.byte.UB = HexRecordSt.Data[0];
				HexRecordSt.ExtSegAddress.byte.HB = HexRecordSt.Data[1];
				HexRecordSt.ExtSegAddress.byte.LB = 0;
				// Reset linear address.
				HexRecordSt.ExtLinAddress.Val = 0;
				break;
				
			case EXT_LIN_ADRS_RECORD:   // Record Type 04, defines 16th to 31st bits of the data address. 
				HexRecordSt.ExtLinAddress.byte.MB = HexRecordSt.Data[0];
				HexRecordSt.ExtLinAddress.byte.UB = HexRecordSt.Data[1];
				HexRecordSt.ExtLinAddress.byte.HB = 0;
				HexRecordSt.ExtLinAddress.byte.LB = 0;
				// Reset segment address.
				HexRecordSt.ExtSegAddress.Val = 0;
				break;
				
			case END_OF_FILE_RECORD:  //Record Type 01, defines the end of file record.
				HexRecordSt.ExtSegAddress.Val = 0;
				HexRecordSt.ExtLinAddress.Val = 0;
				// Disable any interrupts here before jumping to the application.
				//USBDisableInterrupts();
                //SoftReset();
				//JumpToApp();
                return 1;
				break;
				
			default: 
				HexRecordSt.ExtSegAddress.Val = 0;
				HexRecordSt.ExtLinAddress.Val = 0;
				break;
		}		
	}
    return 0;
}	

/********************************************************************
* Function: 	ValidAppPresent()
* Output:		TRUE: If application is valid.
* Overview: 	Logic: Check application vector has 
    some value other than "0xFFFFFF"
********************************************************************/
BOOL ValidAppPresent(void){
	volatile UINT32 *AppPtr;
	
	AppPtr = (UINT32*)USER_APP_RESET_ADDRESS;
	if(*AppPtr == 0xFFFFFFFF){
		return FALSE;
	}else{
		return TRUE;
	}
}			

/****************************************************************************
  Function:
    BOOL USB_ApplicationEventHandler( BYTE address, USB_EVENT event,
                void *data, DWORD size )
  
  Summary:
    This is the application event handler.  It is called when the stack has
    an event that needs to be handled by the application layer rather than
    by the client driver.
  
  Description:
    This is the application event handler.  It is called when the stack has
    an event that needs to be handled by the application layer rather than
    by the client driver.  If the application is able to handle the event, it
    returns TRUE.  Otherwise, it returns FALSE.
  
  Precondition:
    None
  
  Parameters:
    BYTE address    - Address of device where event occurred
    USB_EVENT event - Identifies the event that occured
    void *data      - Pointer to event-specific data
    DWORD size      - Size of the event-specific data
  
  Return Values:
    TRUE    - The event was handled
    FALSE   - The event was not handled
  
  Remarks:
    The application may also implement an event handling routine if it
    requires knowledge of events.  To do so, it must implement a routine that
    matches this function signature and define the USB_HOST_APP_EVENT_HANDLER
    macro as the name of that function.
  ***************************************************************************/

BOOL USB_ApplicationEventHandler( BYTE address, USB_EVENT event, void *data, DWORD size )
{
    switch( event )
    {
        case EVENT_VBUS_REQUEST_POWER:
            // The data pointer points to a byte that represents the amount of power
            // requested in mA, divided by two.  If the device wants too much power,
            // we reject it.
            return TRUE;

        case EVENT_VBUS_RELEASE_POWER:
            // Turn off Vbus power.
            // The PIC24F with the Explorer 16 cannot turn off Vbus through software.

            //This means that the device was removed
            //deviceAttached = FALSE;
            return TRUE;
            break;

        case EVENT_HUB_ATTACH:
            return TRUE;
            break;

        case EVENT_UNSUPPORTED_DEVICE:
            return TRUE;
            break;

        case EVENT_CANNOT_ENUMERATE:
            //UART2PrintString( "\r\n***** USB Error - cannot enumerate device *****\r\n" );
            return TRUE;
            break;

        case EVENT_CLIENT_INIT_ERROR:
            //UART2PrintString( "\r\n***** USB Error - client driver initialization error *****\r\n" );
            return TRUE;
            break;

        case EVENT_OUT_OF_MEMORY:
            //UART2PrintString( "\r\n***** USB Error - out of heap memory *****\r\n" );
            return TRUE;
            break;

        case EVENT_UNSPECIFIED_ERROR:   // This should never be generated.
            //UART2PrintString( "\r\n***** USB Error - unspecified *****\r\n" );
            return TRUE;
            break;

        default:
            break;
    }
    return FALSE;
}


void __ISR(_TIMER_1_VECTOR, IPL2AUTO) Timer1Handler(void) {
    byte i;
    
    INTClearFlag(INT_T1);   // Clear the interrupt flag

    if(timeoutUart)timeoutUart--;
    if(rt10ms1)rt10ms1--;
    if(rt10ms2)rt10ms2--;
    
}

